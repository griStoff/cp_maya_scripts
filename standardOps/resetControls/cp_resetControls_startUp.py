import maya.cmds as mc

import sys
import os.path

newPath= os.path.normpath("C:\\work\\cp_maya_scripts\\cp_maya_scripts\\standardOps\\resetControls\\")

if not newPath in sys.path:
    sys.path.append(newPath)
    
import cp_resetControls
reload (cp_resetControls)

cp_resetControls.reset_controls(mc.ls(sl= True))