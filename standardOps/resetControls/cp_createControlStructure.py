################################################
########
########		to do: seperate create master node from add to master node
########			   code structure where you can switch master nodes
########
################################################




def cp_createMasterNode(inputName):
	import maya.cmds as mc
	import maya.OpenMaya as om
	
	
	cp_selection= mc.ls(sl= True)
	if cp_selection:
		masterNodeName= inputName
		listNamesInScene= mc.ls(inputName)
		
		if len(listNamesInScene) == 0:
			masterNode= mc.createNode('transform', n= masterNodeName)
			
			isMasterNodeAttrName= cp_assign_as_masterNode(masterNodeName)
			
			masterAttrName= cp_add_masterNode_attibutes(masterNodeName)
			
			for sel in cp_selection:
				attrName= '{0}.connectionInformation'.format(sel)
				mc.addAttr(sel, ln= 'connectionInformation', at= 'message')
				mc.connectAttr(masterAttrName, attrName)
			om.MGlobal.displayInfo('created "{0}" node and added selection to it'.format(masterNodeName))
		
		
		elif len(listNamesInScene) == 1:
			masterAttrName= '{0}.connectedControls'.format(masterNodeName)
			
			for sel in cp_selection:
				attrName= '{0}.connectionInformation'.format(sel)
				if not mc.attributeQuery('connectionInformation', node= sel, exists= True):
					mc.addAttr(sel, ln= 'connectionInformation', at= 'message')
				
				connectionListCheck= mc.listConnections( attrName, destination=False, source=True)
				if not connectionListCheck:
					mc.connectAttr(masterAttrName, attrName)
				elif len(connectionListCheck) == 1:
					connectedAttributes= mc.listConnections( attrName, destination=False, source=True, connections= True, plugs= True)
					connectedMaster= connectionListCheck[0]
					
					if not connectedMaster == masterNodeName:
						mc.disconnectAttr(connectedAttributes[1], connectedAttributes[0])
						mc.connectAttr(masterAttrName, attrName)
						
						om.MGlobal.displayInfo('removed selection from {0} and added to "{1}" instead'.format(connectedMaster, masterNodeName))
					else:
						om.MGlobal.displayInfo('added selection to "{0}"'.format(masterNodeName))
				else:
					om.MGlobal.displayWarning('there are multiple connections into "{0}" - interaction aborted since this should never be possible'.format(sel))



		else:
			om.MGlobal.displayInfo('there is more than one node called "{0}" in the scene'.format(masterNodeName))


	else:
		om.MGlobal.displayInfo('nothing was selected')

def cp_assign_as_masterNode(inputString):
	import maya.cmds as mc
	isMasterNodeAttrName= '{0}.isMasterNode'.format(inputString)
	mc.addAttr(inputString, ln= 'isMasterNode', at= 'message', readable= False, writable= False)
	mc.setAttr('{0}.hiddenInOutliner'.format(inputString), 1)

	return isMasterNodeAttrName

def cp_add_masterNode_attibutes(inputString):
	import maya.cmds as mc
	masterAttrName= '{0}.connectedControls'.format(inputString)
	mc.addAttr(ln= 'connectedControls', at= 'message')
	return masterAttrName

def cp_deleteMasterNode(inputName):
	print ('this is to do')

def cp_removeFromMasterNode():
	print ('this is to do')


def cp_switchMasterNode(inputStringOne, inputStringTwo):
	print ('this is to do')

def cp_get_SceneMasterNodes():
	import maya.cmds as mc
	sceneTransformList= mc.ls(transforms= True)
	
	masterNodeList= []
	for transform in sceneTransformList:
		masterNodeCheck = mc.listAttr(transform, string= ['isMasterNode'])
		if masterNodeCheck:
			if len(masterNodeCheck) == 1:
				masterNodeList.append(transform)

	return masterNodeList

def cp_get_masterConnections():
	import maya.cmds as mc
	selectedObject= mc.textScrollList('cp_ui_sceneMasterNodesList', query= True, si= True)
	return selectedObject

def cp_get_slaveConnections(inputNode):
	import maya.cmds as mc
	attrName= '{0}.connectedControls'.format(inputNode)
	connectedSlaves= mc.listConnections( attrName, destination= True, source= False)
	return connectedSlaves


def get_selection_doubleclick(*args):
	mc.textScrollList('cp_ui_sceneMasterNodesList', q= True, append= masterNodes)


def cp_switchToggle(argument):
	switcher = {
		0:1,
		1:0,
	}
	return switcher.get(argument, "None")

def cp_toggleMasterVisibility(*args):
	import maya.cmds as mc
	argument = mc.checkBox('cp_ui_saveVisibilityToggle', q= True, value= True)
	setTrueFalse= cp_switchToggle(argument)

	mc.checkBox('cp_ui_saveVisibilityToggle', e= True, value= setTrueFalse)
	
	masterNodeList= cp_get_SceneMasterNodes()
	if masterNodeList:
		for i in masterNodeList:
			mc.setAttr('{0}.hiddenInOutliner'.format(i), setTrueFalse)

		editors = mc.lsUI(editors=True)
		for ed in editors:
			if mc.outlinerEditor(ed, exists=True):
				mc.outlinerEditor(ed, e=True, refresh=True)
################################## UI


def main():
	import sys
	import os.path
	import maya.cmds as mc
	
	uiFilePath= os.path.normpath("C:\\work\\cp_maya_scripts\\cp_maya_scripts\\standardOps\\resetControls\\cp_createControlStructureUI.ui")
	if mc.window('cp_ui_createControlStructureWindow', exists= True):
		mc.deleteUI('cp_ui_createControlStructureWindow')
	cp_createControlStructureWindow= mc.loadUI(uiFile= uiFilePath)
	masterNodes= cp_get_SceneMasterNodes()

	mc.textScrollList('cp_ui_sceneMasterNodesList', e= True, append= masterNodes)
	mc.textScrollList('cp_ui_sceneMasterNodesList', e= True, selectCommand= cp_ui_getSlaveNodes)
	mc.textField('cp_ui_newMasterName', e= True, enterCommand= cp_ui_createMasterNode, alwaysInvokeEnterCommandOnReturn= True)

	mc.checkBox('cp_ui_resetContextualCheckBox', e= True, annotation= 'this will reset all components by an underlying selection which can be created by this tool')
	mc.checkBox('cp_ui_resetContextualCheckBox', e= True, value= True)

	cp_ui_greyOutCheckBoxes()
	mc.checkBox('cp_ui_resetAllCheckBox', e= True, value= True)
	mc.checkBox('cp_ui_resetAllCheckBox', e= True, onCommand= cp_ui_greyOutCheckBoxes)
	mc.checkBox('cp_ui_resetAllCheckBox', e= True, offCommand= cp_ui_highlightCheckBoxes)

	mc.checkBox('cp_ui_saveVisibilityToggle', e= True, visible= False)

	mc.showWindow(cp_createControlStructureWindow)

def cp_ui_resetControls(*args):
	import sys
	import os.path
	import maya.cmds as mc
	import maya.OpenMaya as om

	newPath= os.path.normpath("C:\\work\\cp_maya_scripts\\cp_maya_scripts\\standardOps\\resetControls\\")

	if not newPath in sys.path:
		sys.path.append(newPath)
		
	import cp_resetControls
	reload (cp_resetControls)
	
	
	
	contextualityCheck= mc.checkBox('cp_ui_resetContextualCheckBox', q= True, value= True)
	transformsList= []
	
	allCheck= mc.checkBox('cp_ui_resetAllCheckBox', q= True, value= True)

	if allCheck:
		transformsList.append('t')
		transformsList.append('r')
		transformsList.append('s')
		cp_resetControls.reset_controls(mc.ls(sl= True), contextuality= contextualityCheck, transformList= transformsList)
	
	else:
		translateCheck= mc.checkBox('cp_ui_resetTranslationCheckBox', q= True, value= True)
		rotateCheck= mc.checkBox('cp_ui_resetRotationCheckBox', q= True, value= True)
		scaleCheck= mc.checkBox('cp_ui_resetScaleCheckBox', q= True, value= True)
		
		if translateCheck or rotateCheck or scaleCheck:
			if translateCheck:
				transformsList.append('t')
			if rotateCheck:
				transformsList.append('r')		
			if scaleCheck:
				transformsList.append('s')
			
			cp_resetControls.reset_controls(mc.ls(sl= True), contextuality= contextualityCheck, transformList= transformsList)
		
		else:
			om.MGlobal.displayWarning('nothing selected to reset, so nothing will be done')



def cp_ui_getSlaveNodes():
	import maya.cmds as mc
	selectedMasterNode= cp_get_masterConnections()[0]
	connectedControls= cp_get_slaveConnections(selectedMasterNode)
	mc.textScrollList('cp_ui_connectedSlaveNodesList', e= True, removeAll= True)
	if connectedControls:
		mc.textScrollList('cp_ui_connectedSlaveNodesList', e= True, append= connectedControls)

def cp_ui_createMasterNode(*args):
	import maya.cmds as mc
	inputName= mc.textField('cp_ui_newMasterName', query= True, text= True)
	
	if inputName != '':
		cp_createMasterNode(inputName)
		mc.textField('cp_ui_newMasterName', e= True, text= '')
		
		mc.textScrollList('cp_ui_sceneMasterNodesList', e= True, removeAll= True)
		
		masterNodes= cp_get_SceneMasterNodes()
		mc.textScrollList('cp_ui_sceneMasterNodesList', e= True, append= masterNodes)
	
	else:
		#checkSelection= mc.textField('cp_ui_newMasterName', query= True, selectItem= True)
		#print checkSelection
		#if 
		print 'nothing selected'
		#selectedMasterNode= mc.textScrollList('cp_ui_sceneMasterNodesList', q= True, select= True)
		#cp_createMasterNode(selectedMasterNode)
	
def cp_ui_greyOutCheckBoxes(*args):
	import maya.cmds as mc
	mc.checkBox('cp_ui_resetTranslationCheckBox', e= True, enable= False)
	mc.checkBox('cp_ui_resetRotationCheckBox', e= True, enable= False)
	mc.checkBox('cp_ui_resetScaleCheckBox', e= True, enable= False)

def cp_ui_highlightCheckBoxes(*args):
	import maya.cmds as mc
	mc.checkBox('cp_ui_resetTranslationCheckBox', e= True, enable= True)
	mc.checkBox('cp_ui_resetRotationCheckBox', e= True, enable= True)
	mc.checkBox('cp_ui_resetScaleCheckBox', e= True, enable= True)

def cp_ui_updateMasterNodeList(*args):
	import maya.cmds as mc
	mc.textScrollList('cp_ui_sceneMasterNodesList', e= True, removeAll= True)
	
	masterNodes= cp_get_SceneMasterNodes()
	mc.textScrollList('cp_ui_sceneMasterNodesList', e= True, append= masterNodes)
'''
def getSelected():
	import maya.cmds as mc
    someList = mc.textScrollList(fooBar, q=1, si=1)
    print someList
'''