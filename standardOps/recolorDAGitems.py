def color_dag_items(items, inColor):
    import maya.cmds as mc
    RGB_values= ['R','G','B']
    for i in items:
        mc.setAttr(i + '.useOutlinerColor', True)
        for v in range(len(RGB_values)):
            mc.setAttr('{}.outlinerColor{}'.format(i, RGB_values[v]), inColor[v])


def format_constraints():
    import maya.cmds as mc
    constraintsList= ['parentConstraint', 'pointConstraint', 'orientConstraint', 'scaleConstraint', 'aimConstraint', 'poleVectorConstraint']
    constraintEndingList= ['prC', 'ptC', 'orC', 'scC', 'aiC', 'pvC']
    for position, constType in enumerate( constraintsList):
        for selection in mc.ls(type= constType):
            
            oldNameList= selection.split('_')
            
            poppedNameList= oldNameList.pop(-1)
            
            newNameWithoutEnding= '_'.join([str(elem) for elem in oldNameList])
            
            newNameWithEnding= '{0}_{1}'.format(newNameWithoutEnding, constraintEndingList[position])
            print (newNameWithEnding)
            mc.rename(selection, newNameWithEnding)
            mc.setAttr('{0}.template'.format(newNameWithEnding), 1)
            mc.setAttr('{0}.visibility'.format(newNameWithEnding), 0)

def format_RIGjoints():
    import maya.cmds as mc
    rig= mc.ls('*_RIG')
    redColor = [.9,.2,.1]
    color_dag_items(rig, redColor)

def format_SKINjoints():
    import maya.cmds as mc
    jnts= mc.ls('*_jnt')
    greenColor = [.1,.9,.2]
    color_dag_items(jnts, greenColor)

def format_ENDjoints():
    import maya.cmds as mc
    endJnts= mc.ls('*_END')
    hardRed= [1.0,0.1,0.1]
    color_dag_items(endJnts, hardRed)