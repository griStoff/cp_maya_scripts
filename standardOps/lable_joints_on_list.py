def lable_joints_of_list(inputList, connectDrawlabel= False):
    """ documentation """
    import maya.cmds as mc

    allowedPrefixes= ['Lf_', 'L_', 'Rt_', 'R_', 'Cr_', 'C_']
    
    for i in inputList:
        typeCheck= cmds.objectType( i, isType='joint' )
        
        if typeCheck:
            print '{0} is a joint'.format(i)
            if '_' in i:
                
                #### if it does not work on fringe cases try with index + copy etc
                splitString= i.split('_')
                
                middleString = splitString[1]
                
                lengthCheck= len(splitString)
                if lengthCheck > 3:
                    middleString='{0}_{1}'.format(middleString, splitString[2])
                
                formatedPrefix= '{0}_'.format(splitString[0])
                formatedPrefix= formatedPrefix.capitalize()
                print formatedPrefix[0]
                
                if formatedPrefix in allowedPrefixes:
                    
                    mc.setAttr('{name}.type'.format(name= i), 18)
                    mc.setAttr('{name}.otherType'.format(name= i), middleString, type= 'string')
                    
                    print 'prefix of it is "{0}"'.format(formatedPrefix)
                    
                    if formatedPrefix[0] == 'L':
                        mc.setAttr('{name}.side'.format(name= i), 1)
                    
                    if formatedPrefix[0] == 'R':
                        mc.setAttr('{name}.side'.format(name= i), 2)

                    if formatedPrefix[0] == 'C':
                        mc.setAttr('{name}.side'.format(name= i), 0)
                    
                    if connectDrawlabel == True:
                        print '"TO BE ADDED"'
                        
                else:
                    mc.warning( '"{0}" is not a valid prefix'.format(formatedPrefix))
            else:
                print 'ERROR: {0} has no "_"'.format(i)    
        else:
            print 'ERROR: {0} is not'.format(i)

def main():
	lable_joints_of_list(mc.ls(sl=True))