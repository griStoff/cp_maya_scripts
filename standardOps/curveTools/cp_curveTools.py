def main():
    import sys
    import os.path
    import maya.cmds as mc
    
    uiFilePath= os.path.normpath("C:\\work\\cp_maya_scripts\\cp_maya_scripts\\standardOps\\curveTools\\cp_curveToolsUI.ui")
    if mc.window('cp_curveToolsWindow', exists= True):
        mc.deleteUI('cp_curveToolsWindow')
    window= mc.loadUI(uiFile= uiFilePath)

    cp_curveInputPath= os.path.normpath('C:\\work\\cp_maya_scripts\\setup\\curves')
    mc.textField('cp_curveTools_path', e= True, placeholderText= cp_curveInputPath)
    cp_listAllMayaCurves()
    mc.showWindow(window)

def cp_getCurveInfo():
    import maya.cmds as mc
    selectionList= mc.ls(sl= True)
    
    if selectionList:
        
        selectedItem= selectionList[0]
        shapeNodes= mc.listRelatives(shapes= True)
        
        if shapeNodes:
            
            if mc.objectType(shapeNodes, isType= 'nurbsCurve'):
                print "'it is a nurbsCurve'"
                
                degrees= mc.getAttr('{0}.degree'.format(selectedItem))
                curveCvList= mc.ls('{0}.cv[0:]'.format(selectedItem), fl=True)
                
                curvePointList= []
                for cv in curveCvList:
                    cvPosition= tuple(mc.pointPosition(cv, world= True))
                    curvePointList.append(cvPosition)
                
                return [degrees, curvePointList]
                
            else:
                print "'select a nurbsCurve'"
                return None
        else:
            print "'this transform node has no shape'"
            return None
    else:
        print "'nothing is done, because nothing is selected'"
        return None


def cp_loadJsonCurve(objectToGet, fileName):
    import json as js
    if ".json" not in fileName:
        fileName += ".json"

    print "> read curve from json file is seeing: {0}".format(fileName)
    
    with open(fileName, "r") as jsonFile:
        loadedFile= js.load(jsonFile)
        newDegreeInt= loadedFile[objectToGet][0]
        newPointList= [tuple(i) for i in loadedFile[objectToGet][1]]
        
    return newDegreeInt, newPointList


def cp_writeJsonFile(inputName, inputList, fileName):
    import json as js
    if ".json" not in fileName:
        fileName += ".json"
        
    with open(fileName, "r") as jsonFile:
        loadedFile= js.load(jsonFile)

        if inputName not in loadedFile:
            
            newData= {inputName: inputList}
            print newData
            print loadedFile
            
            loadedFile[inputName]= inputList
            print loadedFile
            print "> write to json file is seeing: {0}".format(fileName)
            
            
            with open(fileName, "w") as jsonFile:
                js.dump(loadedFile, jsonFile, indent=2)
            print "Data was successfully written to {0}".format(fileName)
        else: 
            print '> "{0}" already exists in json file: {1}'.format(inputName, fileName)
    return fileName

def cp_deleteJsonCurve(inputName, fileName):
    import json as js
    if ".json" not in fileName:
        fileName += ".json"
        
    with open(fileName, "r") as jsonFile:
        loadedFile= js.load(jsonFile)

        if inputName in loadedFile:
            
            loadedFile.pop(inputName, None)
            print "> write to json file is seeing: {0}".format(fileName)
            
            
            with open(fileName, "w") as jsonFile:
                js.dump(loadedFile, jsonFile, indent=2)
            print "Data was successfully written to {0}".format(fileName)
        else: 
            print '> "{0}" already exists in json file: {1}'.format(inputName, fileName)
    return fileName

#~~~~~~~~~~~~~~~~~~~~~~~~~~~UI OPERATIONS
def cp_getInputPath():
    import sys
    import os.path
    import maya.cmds as mc

    cp_curveInputPath= mc.textField('cp_curveTools_path', q= True, placeholderText= True)
    print cp_curveInputPath
    return cp_curveInputPath

def cp_updateInputPath():
    import sys
    import os.path
    import maya.cmds as mc
    cp_curveInputPathGet= mc.textField('cp_curveTools_path', q= True, text= True)
    if len(cp_curveInputPathGet)!= 0:
        cp_curveInputPathSet= mc.textField('cp_curveTools_path', e= True, placeholderText= cp_curveInputPathGet)
        cp_curveInputPathReset= mc.textField('cp_curveTools_path', e= True, text= '')

def cp_listAllJsonCurves(fileName):
    import json as js
    if ".json" not in fileName:
        fileName += ".json"
        
    with open(fileName, "r") as jsonFile:
        loadedFile= js.load(jsonFile)
        return loadedFile.keys()

def cp_listAllMayaCurves():
    import json as js
    import maya.cmds as mc
    fileName= mc.textField('cp_curveTools_path', q= True, placeholderText= True)
    if ".json" not in fileName:
        fileName += ".json"
        
    with open(fileName, "r") as jsonFile:
        loadedFile= js.load(jsonFile)
        curveList= loadedFile.keys()
        print curveList
    mc.textScrollList('cp_curveTools_list', e= True, removeAll= True)
    mc.textScrollList('cp_curveTools_list', e= True, append= curveList)

def cp_deleteMayaCurve():
    import json as js
    import maya.cmds as mc
    inputName= mc.textField('cp_curveTools_name', q= True, text= True)
    selectedItemName= mc.textScrollList('cp_curveTools_list', q= True, selectItem= True)
    fileName= mc.textField('cp_curveTools_path', q= True, placeholderText= True)
    if inputName != '':
        if ".json" not in fileName:
            fileName += ".json"
        with open(fileName, "r") as jsonFile:
            loadedFile= js.load(jsonFile)

            if inputName in loadedFile:
                
                loadedFile.pop(inputName, None)
                print "> write to json file is seeing: {0}".format(fileName)
                curveList= loadedFile.keys()
                mc.textScrollList('cp_curveTools_list', e= True, removeAll= True)
                mc.textScrollList('cp_curveTools_list', e= True, append= curveList)
                
                with open(fileName, "w") as jsonFile:
                    js.dump(loadedFile, jsonFile, indent=2)
                print "Data was successfully written to {0}".format(fileName)
            else: 
                print '> "{0}" already exists in json file: {1}'.format(inputName, fileName)
    else:
        if selectedItemName != None:
            if ".json" not in fileName:
                fileName += ".json"
            with open(fileName, "r") as jsonFile:
                loadedFile= js.load(jsonFile)

                if selectedItemName[0] in loadedFile:
                    
                    loadedFile.pop(selectedItemName[0], None)
                    print "> write to json file is seeing: {0}".format(fileName)
                    curveList= loadedFile.keys()
                    mc.textScrollList('cp_curveTools_list', e= True, removeAll= True)
                    mc.textScrollList('cp_curveTools_list', e= True, append= curveList)
                    
                    with open(fileName, "w") as jsonFile:
                        js.dump(loadedFile, jsonFile, indent=2)
                    print "Data was successfully written to {0}".format(fileName)
                else: 
                    print '> "{0}" already exists in json file: {1}'.format(selectedItemName[0], fileName)
    mc.textField('cp_curveTools_name', e= True, text= '')

def cp_writeMayaFile():
    import maya.cmds as mc
    import json as js
    selectionList= mc.ls(sl= True)
    fileName= mc.textField('cp_curveTools_path', q= True, placeholderText= True)
    inputName= mc.textField('cp_curveTools_name', q= True, text= True)
    if inputName != '':
        if selectionList:
            
            selectedItem= selectionList[0]
            shapeNodes= mc.listRelatives(shapes= True)
            
            if shapeNodes:
                
                if mc.objectType(shapeNodes, isType= 'nurbsCurve'):
                    print "'it is a nurbsCurve'"
                    
                    degrees= mc.getAttr('{0}.degree'.format(selectedItem))
                    curveCvList= mc.ls('{0}.cv[0:]'.format(selectedItem), fl=True)
                    
                    curvePointList= []
                    for cv in curveCvList:
                        cvPosition= tuple(mc.pointPosition(cv, world= True))
                        curvePointList.append(cvPosition)
                    
                    curveInformation= [degrees, curvePointList]
                    
                else:
                    print "'select a nurbsCurve'"
            else:
                print "'this transform node has no shape'"
        else:
            print "'nothing is done, because nothing is selected'"

        if ".json" not in fileName:
            fileName += ".json"
            
        with open(fileName, "r") as jsonFile:
            loadedFile= js.load(jsonFile)

            if inputName not in loadedFile:
                
                newData= {inputName: curveInformation}
                print newData
                print loadedFile
                
                loadedFile[inputName]= curveInformation
                print loadedFile
                print "> write to json file is seeing: {0}".format(fileName)
                
                
                with open(fileName, "w") as jsonFile:
                    js.dump(loadedFile, jsonFile, indent=2)
                print "Data was successfully written to {0}".format(fileName)
            else: 
                print '> "{0}" already exists in json file: {1}'.format(inputName, fileName)
    else:
        print 'test'
    curveList= loadedFile.keys()
    mc.textScrollList('cp_curveTools_list', e= True, removeAll= True)
    mc.textScrollList('cp_curveTools_list', e= True, append= curveList)

def cp_createMayaCurve():
    import maya.cmds as mc
    import json as js
    inputName= mc.textField('cp_curveTools_name', q= True, text= True)
    selectedItemName= mc.textScrollList('cp_curveTools_list', q= True, selectItem= True)
    fileName= mc.textField('cp_curveTools_path', q= True, placeholderText= True)
    if ".json" not in fileName:
        fileName += ".json"

    print "> read curve from json file is seeing: {0}".format(fileName)
    
    with open(fileName, "r") as jsonFile:
        if inputName != '':

            loadedFile= js.load(jsonFile)
            newDegreeInt= loadedFile[inputName][0]
            newPointList= [tuple(i) for i in loadedFile[inputName][1]]
            mc.curve(n= inputName, d= newDegreeInt, p= newPointList)
            
        else:
            if selectedItemName != None:
                loadedFile= js.load(jsonFile)
                newDegreeInt= loadedFile[selectedItemName[0]][0]
                newPointList= [tuple(i) for i in loadedFile[selectedItemName[0]][1]]
                mc.curve(n= selectedItemName[0], d= newDegreeInt, p= newPointList)
'''
# Select curves in the viewport add make them renderable in V-Ray.

import maya.cmds as cmds
import maya.OpenMaya as om

# Convert the selection from transforms to shapes
transforms = cmds.ls(tr=True, sl=True)
shapes = cmds.listRelatives(transforms, s=True)

# Set the values you want for your renderable curves
Tesselation = 2
StartWidht = 0.015

# Enter the name of the shader you like to apply to your curves
shader = "lambert1"

for s in shapes:
    cmds.select(s, r=True)
    
    cmds.vray("addAttributesFromGroup", s, "vray_nurbscurve_renderable", 1)
    cmds.getAttr(".vrayNurbsCurveRenderable")
    cmds.setAttr(".vrayNurbsCurveRenderable", 1)
    cmds.getAttr(".vrayNurbsCurveMaterial")
    cmds.connectAttr(shader + ".outColor", ".vrayNurbsCurveMaterial")
    
    cmds.getAttr(".vrayNurbsCurveTesselation")
    cmds.setAttr(".vrayNurbsCurveTesselation", .8)
    cmds.getAttr(".vrayNurbsCurveStartWidth")
    cmds.setAttr(".vrayNurbsCurveStartWidth", .05)
    cmds.select(s, d=True)
    cmds.setAttr()
    om.MGlobal.displayInfo("Renderable Curves added to selection")


#mc.curve(d= 1, p= loadJsonCurve('ikCurve', "C:\\work\\cp_maya_scripts\\setup\\curves"))
'''