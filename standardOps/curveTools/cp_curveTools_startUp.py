import sys
import os.path

newPath= os.path.normpath("C:\\work\\cp_maya_scripts\\cp_maya_scripts\\standardOps\\curveTools")

if not newPath in sys.path:
    sys.path.append(newPath)
    
import cp_curveTools
reload (cp_curveTools)

cp_curveTools.main()
