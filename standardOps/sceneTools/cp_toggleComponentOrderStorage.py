def SwitchExample(argument):
    switcher = {
        0: True,
        1: False,
    }
    return switcher.get(argument, "nothing")

def main():
    import maya.cmds as mc
    import maya.OpenMaya as om

    def SwitchExample(argument):
        switcher = {
            0: True,
            1: False,
        }
        return switcher.get(argument, "nothing")


    argument = mc.selectPref(tso=True, q=True)
    
    setTrueFalse= SwitchExample(argument)
    
    mc.selectPref(tso= setTrueFalse)

    if setTrueFalse:
        om.MGlobal.displayInfo('component selection will be stored - this may cause a slowdown')
    else:
        om.MGlobal.displayInfo('component selection is disabled - order of components will not be stored')