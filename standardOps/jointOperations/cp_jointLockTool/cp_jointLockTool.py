def cp_jointLock():
    import maya.cmds as mc
    for s in mc.ls(type= 'joint'):
        try:
            mc.setAttr('{0}.liw'.format(s), 1)
        except:
            continue
    
    cp_selectedJointsList= mc.ls(sl= True)
    if len(cp_selectedJointsList)>0:
        for s in cp_selectedJointsList:
            if mc.objectType(s, isType= 'joint'):
                mc.setAttr('{0}.liw'.format(s), 0)
    else:
        for s in mc.ls(type= 'joint'):
            try:
                mc.setAttr('{0}.liw'.format(s), 0)
            except:
                continue
def main():
    cp_jointLock()