
import sys
import os.path

newPath= os.path.normpath("C:\\work\\cp_maya_scripts\\cp_maya_scripts\\standardOps\\jointOperations\\cp_jointLockTool")

if not newPath in sys.path:
    sys.path.append(newPath)
    
import cp_jointLockTool
reload (cp_jointLockTool)

cp_jointLockTool.main()