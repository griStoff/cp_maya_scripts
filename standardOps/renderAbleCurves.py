# Select curves in the viewport add make them renderable in V-Ray.

import maya.cmds as cmds
import maya.OpenMaya as om

# Convert the selection from transforms to shapes
transforms = cmds.ls(tr=True, sl=True)
shapes = cmds.listRelatives(transforms, s=True)
# Set the values you want for your renderable curves
Tesselation = 2
StartWidht = 0.015

# Enter the name of the shader you like to apply to your curves
shader = "cables_vMat"

for s in shapes:
    if 'Orig' not in s:
        
        cmds.select(s, r=True)
        
        try:
            cmds.vray("addAttributesFromGroup", s, "vray_nurbscurve_renderable", 1)
            cmds.getAttr(".vrayNurbsCurveRenderable")
            cmds.setAttr(".vrayNurbsCurveRenderable", 1)
            cmds.getAttr(".vrayNurbsCurveMaterial")
            cmds.connectAttr(shader + ".outColor", ".vrayNurbsCurveMaterial")
        
        except:
            pass
        
        cmds.getAttr(".vrayNurbsCurveTesselation")
        cmds.setAttr(".vrayNurbsCurveTesselation", .8)
        cmds.getAttr(".vrayNurbsCurveStartWidth")
        cmds.setAttr(".vrayNurbsCurveStartWidth", .2)
        cmds.select(s, d=True)
    
    
    else:
        cmds.select(s, r=True)
        
        try:
            cmds.vray("addAttributesFromGroup", s, "vray_nurbscurve_renderable", 0)
        
        except:
            pass
    #cmds.setAttr()
        om.MGlobal.displayInfo("Renderable Curves added to selection")