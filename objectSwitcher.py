import maya.cmds as cmds
import random

def switchObjs():
    # get selection --> Selektion abrufen   # Vorgehensweise fuer Code notieren
    selGrp = cmds.ls( sl = True )              # sl = selection
    sel = cmds.listRelatives( selGrp[0], children = True )
    #grpName = cmds.listRelatives( sel[0], parent = True )

    # get pos of each item of my selection
    for i in range( len(sel) ):   # range = Listenlaenge
        origPos = cmds.xform( sel[i], q=True, t=True )    # [Index] , i steht fuer eine Zahl, die in jedem Durchgang anders ist, q=abfragen
    
        # create new sphere
        newSphere = cmds.polySphere()
        #newCube = cmds.polyCube()
        
        # position sphere
        origScl = cmds.xform( sel[i], q=True, s=True )
        #cmds.xform( newSphere[0], r=True, t=(origPos), s=(origScl))  # auch t=origPos moeglich, da () nur bei mehreren Attributen noetig
        
        #origRot = cmds.xform( sel[i], q=True, ro=True )
        #cmds.xform( newCube[0], r=True, t=(origPos), s=(3,5,2),ro=(30,50,20))
        
        # put sphere in group
        cmds.parent(newSphere[0], selGrp[0] )   # parent sortiert erzeugte Objekte in bestehende Gruppe ein
        #cmds.parent(newSphere[0], grpName[0] )
        
        # hide/delete original object
    cmds.hide( sel )
        
    