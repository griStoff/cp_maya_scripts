import maya.cmds as mc
import math
import random



def main():
	import sys
	import os.path
	import maya.cmds as mc
	
	uiFilePath= os.path.normpath("C:\\work\\cp_maya_scripts\\cp_maya_scripts\\objectPlacerUI.ui")
	if mc.window('objectPlacer', exists= True):
		mc.deleteUI('objectPlacer')
	
	window= mc.loadUI(uiFile= uiFilePath)
	mc.showWindow(window)




def fillObjList():
	import maya.cmds as mc
	
	selectionList= mc.ls(sl= True)
	checkerList=[]
	selectedItems= mc.textScrollList('cp_inputList', query= True, allItems= True)
	
	if selectedItems != None:
		print selectedItems
		selectionList= [x for x in selectionList if x not in selectedItems]
	else:
		pass

	mc.textScrollList('cp_inputList', edit= True, append= selectionList)

def removeObjList():
	import maya.cmds as mc
	selectedItems= mc.textScrollList('cp_inputList', query= True, selectItem= True)
	if selectedItems != None:
		for i in selectedItems:
			mc.textScrollList('cp_inputList', edit= True, removeItem= i)


def removeAll():
	mc.textScrollList('cp_inputList', edit= True, removeAll= True)






def queryFieldInteger(input):
	import maya.cmds as mc
	
	
	output= mc.textField(input, q= True, text= True)
	
	
	if output.isdecimal():
		output= int(output)
		
		if not output:
			mc.confirmDialog( title='Confirm', message='please select not zero', button=['i promise'], defaultButton='Yes')
			output= 0
	
	else:
		mc.confirmDialog( title='Confirm', message='please select a number', button=['i promise'], defaultButton='Yes')
		output= 0
	
	return output

def queryFieldString(input):
	import maya.cmds as mc
	
	output= mc.textField(input, q= True, text= True)
	
	
	if len(output) != 0:
		if output[0].isdecimal():
			mc.confirmDialog( title='Confirm', message='do not start a name with a number', button=['i promise'])
			output= 0
	else:
		mc.confirmDialog( title='Confirm', message='no name was selected, default will be used', button=['i understand'])
		output= 'defaultName'

	return output

def queryObjList():
	import maya.cmds as mc
	selectedItems= mc.textScrollList('cp_inputList', query= True, selectItem= True)

	if selectedItems is None:
		selectedItems=[]

	return selectedItems





def createObjs(inputNumber, inputItems=[], inputName= 'randomCube'):
	import math
	import random
	import maya.cmds as mc
	
	amount= inputNumber
	groupName= '{0}_grp'.format(inputName)
	operationGroup= []
	
	
	grpName = mc.group( em=True, name=groupName)
	
	
	if inputItems == []:
		print 'ayyyyyyyyyy'
		for i in range(amount):
			specName= 'lol'
			
			
			newCube = mc.polyCube() 
			x = random.randint (-10, 10)
			y = random.randint (-10, 10)
			z = random.randint (-10, 10)
			mc.xform( newCube[0], r=True, t=(x, y, z))
			mc.parent( newCube[0], grpName )
	else:
		print 'nayyyyy'
		listLength= len(inputItems)-1
		print listLength
		
		for i in range(amount):
			specName= '{0}_{1}'.format(inputName, str(i).zfill(len(str(amount))))
			
			newCube = mc.duplicate(inputItems[random.randint(0,listLength)], n= specName)
			x = random.randint (-10, 10)
			y = random.randint (-10, 10)
			z = random.randint (-10, 10)
			mc.xform( newCube[0], r=True, t=(x, y, z))
			mc.parent( newCube[0], grpName )

def execute():
	name= queryFieldString('inputName')
	amount= queryFieldInteger('inputAmount')
	objectList= queryObjList()
	print objectList
	if amount != 0:
		createObjs(amount, objectList, name)
'''

def switchObjs():
	import math
	import random
    # get selection --> Selektion abrufen   # Vorgehensweise fuer Code notieren
    selGrp = cmds.ls( sl = True )              # sl = selection
    sel = cmds.listRelatives( selGrp[0], children = True )
    #grpName = cmds.listRelatives( sel[0], parent = True )

    # get pos of each item of my selection
    for i in range( len(sel) ):   # range = Listenlaenge
        origPos = cmds.xform( sel[i], q=True, t=True )    # [Index] , i steht fuer eine Zahl, die in jedem Durchgang anders ist, q=abfragen
    
        # create new sphere
        newSphere = cmds.polySphere()
        #newCube = cmds.polyCube()
        
        # position sphere
        origScl = cmds.xform( sel[i], q=True, s=True )
        #cmds.xform( newSphere[0], r=True, t=(origPos), s=(origScl))  # auch t=origPos moeglich, da () nur bei mehreren Attributen noetig
        
        #origRot = cmds.xform( sel[i], q=True, ro=True )
        #cmds.xform( newCube[0], r=True, t=(origPos), s=(3,5,2),ro=(30,50,20))
        
        # put sphere in group
        cmds.parent(newSphere[0], selGrp[0] )   # parent sortiert erzeugte Objekte in bestehende Gruppe ein
        #cmds.parent(newSphere[0], grpName[0] )
        
        # hide/delete original object
    cmds.hide( sel )
        
'''