def cp_getCamerasInScene():
    import maya.cmds as mc
    camerasInScene= mc.ls(cameras= True)
    cameraList= []
    for i in camerasInScene:
        cameraList.append(i[0])
    
def cp_loadCamerasFromFile(objectToGet, fileName):
    import json as js
    if ".json" not in fileName:
        fileName += ".json"

    print "> read cams from json file is seeing: {0}".format(fileName)
    
    with open(fileName, "r") as jsonFile:
        loadedFile= js.load(jsonFile)
        newDegreeInt= loadedFile[objectToGet][0]
        
    return cameraNames


def cp_writeCamerasToFile(inputName, inputList, fileName):
    import json as js
    if ".json" not in fileName:
        fileName += ".json"
        
    with open(fileName, "r") as jsonFile:
        loadedFile= js.load(jsonFile)

        if inputName not in loadedFile:
            
            newData= inputName
            print newData
            print loadedFile
            
            = 
            print loadedFile
            print "> write to json file is seeing: {0}".format(fileName)
            
            
            with open(fileName, "w") as jsonFile:
                js.dump(loadedFile, jsonFile, indent=2)
            print "Data was successfully written to {0}".format(fileName)
        else: 
            print '> "{0}" already exists in json file: {1}'.format(inputName, fileName)
    return fileName

def cp_deleteJsonCurve(inputName, fileName):
    import json as js
    if ".json" not in fileName:
        fileName += ".json"
        
    with open(fileName, "r") as jsonFile:
        loadedFile= js.load(jsonFile)

        if inputName in loadedFile:
            
            loadedFile.pop(inputName, None)
            print "> write to json file is seeing: {0}".format(fileName)
            
            
            with open(fileName, "w") as jsonFile:
                js.dump(loadedFile, jsonFile, indent=2)
            print "Data was successfully written to {0}".format(fileName)
        else: 
            print '> "{0}" already exists in json file: {1}'.format(inputName, fileName)
    return fileName