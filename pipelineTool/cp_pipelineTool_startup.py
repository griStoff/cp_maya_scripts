import maya.cmds as mc

import sys
import os.path

newPath= os.path.normpath("C:\\work\\cp_maya_scripts\\cp_maya_scripts\\pipelineTool")


if not newPath in sys.path:
    sys.path.append(newPath)
    print 'added new path'
else:
    print 'it already happended'
    
    
import cp_pipelineTool
reload(cp_pipelineTool)

cp_pipelineTool.main()