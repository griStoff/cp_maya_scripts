SET cp_mayaPath=C:\PROGRA~1\Autodesk\Maya2020\
SET cp_projectPath=C:\work\projects\renderingAbgabenM20\MMAM3AR2IL_Semesterprojekt_WS2020\
SET cp_sceneName=scene_start_04
SET cp_sceneNameWhole=%cp_sceneName%.ma

SET cp_renderCamera=renderCam_shot_02_04
SET cp_renderLayer=foreground_layer

SET cp_compoundedVrayName=%cp_sceneName%__%cp_renderCamera%__%cp_renderLayer%

SET cp_zDepthBlackPoint=50
SET cp_zDepthWhitePoint=465


SET /a cp_startFrame=1
SET /a cp_endFrame=1
SET /a cp_frameStep=1


"%cp_mayaPath%bin\Render.exe" -r vray -s %cp_startFrame% -e %cp_endFrame% -b %cp_frameStep% -cam %cp_renderCamera% -rl %cp_renderLayer% -exportFileName "%cp_projectPath%\cache\%cp_compoundedVrayName%.vrscene" -noRender "%cp_projectPath%scenes\%cp_sceneNameWhole%" 

"%cp_mayaPath%vray\bin\vray.exe" -sceneFile="%cp_projectPath%\cache\%cp_compoundedVrayName%.vrscene" -imgFile= "%cp_projectPath%\images\%cp_compoundedVrayName%.exr" -frames %cp_startFrame%-%cp_endFrame%,%cp_frameStep% -parameterOverride="SettingsImageSampler::type=1" -parameterOverride="SettingsImageSampler::dmc_threshold=2" -parameterOverride="RenderChannelZDepth::depth_black=%cp_zDepthBlackPoint%" -parameterOverride="RenderChannelZDepth::depth_white=%cp_zDepthWhitePoint%" -detailedTiming= 1