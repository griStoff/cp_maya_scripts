import maya.cmds as mc
import os
import functools

def main():
	import sys
	import os.path
	import maya.cmds as mc

	uiFilePath= os.path.normpath("C:\\work\\cp_maya_scripts\\cp_maya_scripts\\pipelineTool\\cp_pipelineTool.ui")
	if mc.window('cp_pipelineToolWindow', exists= True):
		mc.deleteUI('cp_pipelineToolWindow')

	window= mc.loadUI(uiFile= uiFilePath)


	cp_rootPath= get_proj_root_path()
	categoryFolders= os.listdir(cp_rootPath)
	print categoryFolders

	set_variables_on_list('cp_projectList', categoryFolders)
	set_variables_on_optionBox('cp_categoryList')
	cp_rootPath+
	set_variables_on_list('cp_projectList', categoryFolders)

	mc.showWindow(window)


def get_proj_root_path():
	rootPath= os.path.normpath("C:\\work\\projects")
	return rootPath

def set_variables_on_list(inputItem, inputList):
	mc.textScrollList(inputItem, edit= True, append= inputList)
	mc.textScrollList(inputItem, edit= True, selectCommand= functools.partial(fill_list, inputItem))

def set_variables_on_optionBox(inputItem):
	mc.optionMenu(inputItem, edit= True, changeCommand= functools.partial(fill_dropDown, inputItem))

def fill_list(inputItem):
	print str('filledList')
	selItem = mc.textScrollList(inputItem, query= True, selectItem= True)[0]

	projectPath= get_proj_root_path()
	scenePath= os.path.join(projectPath, selItem)

	print scenePath
	return scenePath

def refresh_fill_list(inputItem):
	mc.textScrollList(inputItem, query= True)


def fill_dropDown(inputItem):
	print str('filledDropdown')+ str(inputItem)
'''
def query_fields():


'''