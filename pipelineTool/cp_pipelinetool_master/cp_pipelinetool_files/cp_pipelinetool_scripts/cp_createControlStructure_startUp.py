import maya.cmds as mc

import sys
import os.path

newPath= os.path.normpath("C:\work\cp_maya_scripts\cp_maya_scripts\pipelineTool\cp_pipelinetool_master\cp_pipelinetool_files\cp_pipelinetool_scripts")

if not newPath in sys.path:
    sys.path.append(newPath)
    
import cp_createControlStructure
reload (cp_createControlStructure)

cp_createControlStructure.main()