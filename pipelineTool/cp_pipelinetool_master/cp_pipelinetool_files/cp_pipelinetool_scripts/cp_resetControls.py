##################################################################################################################
#
#                            cp_resetControls
#
##################################################################################################################

##################################################################################################################
# how to use:
#
# copy this into a shelfbutton and add the function on the bottom   reset_controls(mc.ls(sl= True), contextuality= True)
#
#
###################################################################################################################


import maya.cmds as mc
import maya.OpenMaya as om




def reset_controls(inputNode, contextuality= True, transformList= ['t','r','s']):
    """ this resets control based on message attributes """
    transformList= transformList
    def resetAxisFromInput(inputNode):
        import maya.cmds as mc
        
        axesList= ['x','y','z']
        
        for axes in axesList:
            for trans in transformList:
                #print ('{node}'.format(node= inputNode) +'.'+ '{transform}{ending}'.format(transform= trans, ending= axes))
                value= mc.attributeQuery('{transform}{ending}'.format(transform= trans, ending= axes), node='{node}'.format(node= inputNode), listDefault=True)[0]
                try:
                    mc.setAttr('{node}.{transform}{ending}'.format(node= inputNode, transform= trans, ending= axes), value)
                
                except: 
                    pass

        
        customAttrs= mc.listAttr(userDefined= True, write= True, settable= True)
        for attr in customAttrs:
            try:
                value= mc.attributeQuery('{attibute}'.format(attibute= attr), node='{node}'.format(node= inputNode), listDefault=True)[0]
                mc.setAttr('{node}.{ending}'.format(node= inputNode, ending= attr), value)
            except:
                pass

    def resetNodeFromList(incomingConnenctionsList):
        """ this resets controls based on message attributes and their master node """

        matchingMasterNodeList = [i for i in incomingConnenctionsList if 'connectedControls' in i]
        #print 'matchingMasterNodeList' +' is: '+str(matchingMasterNodeList)



        if len(matchingMasterNodeList) >= 1:
        
            singleOutNodeList= [i.replace('.connectedControls', '') for i in matchingMasterNodeList]
            print 'singleOutNodeList' +' is: '+str(singleOutNodeList)           
        
            for masterNode in matchingMasterNodeList:
                connectedNodesList= mc.listConnections(masterNode, source= True)
                #print 'connectedNodesList' +' is: '+str(connectedNodesList)

                for control in connectedNodesList:
                    resetAxisFromInput(control)

            newNodeList= [mc.listConnections(i, source= False, destination= True, plugs= True) for i in singleOutNodeList]
            if newNodeList:
                totalMatchingNodeList= []
                for iteration, nodeList in enumerate(newNodeList):
                    underNodesList= nodeList
                    print 'underNodesList' +' is: '+str(underNodesList)
                    matchingMasterNodeList= [i.replace('.connectionInformation', '') for i in underNodesList if 'connectionInformation' in i]
                    print 'matchingMasterNodeListNew' +' is: '+str(matchingMasterNodeList)
                    

                    for checkedNode in matchingMasterNodeList:
                        connectedControlsExistenceCheck= mc.attributeQuery('connectedControls', node= checkedNode, longName= True, exists= True)
                        print 'i do this' + str(checkedNode)

                        if connectedControlsExistenceCheck== 'connectedControls':
                            print iteration
                            connectedControlsConnectionCheck= mc.isConnected('{node}.{attribute}'.format(node= singleOutNodeList[iteration], attribute= 'connectedControls'), '{node}.{attribute}'.format(node= checkedNode, attribute= 'connectionInformation'))
                            print 'connectedControlsConnectionCheck' +' is: '+ str(connectedControlsConnectionCheck)
                            
                            
                            if connectedControlsConnectionCheck:
                                totalMatchingNodeList.append('{node}.{attribute}'.format(node= checkedNode, attribute= 'connectedControls'))
                                print ' | stuff is am happenen | '+ str(totalMatchingNodeList)
                            
                            else:
                                pass
                        
                        else:
                            pass
                print 'totalMatchingNodeList' +' is: '+str(totalMatchingNodeList)
                print '-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-'
                return totalMatchingNodeList
        else:
            om.MGlobal.displayInfo("there is no more master node connected")
            return None


    def resetContextual(inputNode):
        """ information to be put here """
        for i in inputNode:
            incomingConnenctionsList= mc.listConnections(i, source= True, destination= False, plugs= True)
            #~~~~~~~~~~~~~~~ check if there even are incoming connections
            if incomingConnenctionsList:
                nextLowerMasterNode= resetNodeFromList(incomingConnenctionsList)
                #print nextLowerMasterNode

            else:
                nextLowerMasterNode= None
            
            #~~~~~~~~~~~~~~~ check if there is a master node connected
            if not nextLowerMasterNode:
                resetAxisFromInput(i)
            
            while nextLowerMasterNode:
                nextLowerMasterNode= resetNodeFromList(nextLowerMasterNode)
            else:
                break
            
        #mc.select(clear=True)


    def resetSimple(inputNode):
        for i in inputNode:
            resetAxisFromInput(i)
        #mc.select(clear=True)



    if contextuality == True:
        resetContextual(inputNode)
    else:
        resetSimple(inputNode)



def main():
    reset_controls(mc.ls(sl=True), contextuality= True)