import maya.cmds as mc
import os
from functools import partial

#function executed when dropped in scene
def onMayaDroppedPythonFile(*args):

    cp_changeScriptsInDirectory('cp_createControlStructure', cp_fillOutLocations()[2])
    cp_changeScriptsInDirectory('cp_createControlStructure_startUp', cp_fillOutLocations()[2])
    cp_addButton(label= 'cp_createControlStructure_startUp', icon= cp_getIcon('cp_createControlStructure_startUp'), command= cp_readCommandSimple('cp_createControlStructure_startUp'))  
    
    
    cp_changeScriptsInDirectory('cp_resetControls', cp_fillOutLocations()[2])
    cp_changeScriptsInDirectory('cp_resetControls_startUp', cp_fillOutLocations()[2])
    cp_addButton(label= 'cp_resetControls_startUp', icon= cp_getIcon('cp_resetControls_startUp'), command= cp_readCommandSimple('cp_resetControls_startUp'))  

#operationalStuff
def cp_getMayaVersion():
    cp_mayaVersion= mc.about(v=True)

    return cp_mayaVersion

def cp_getFilePath():
    filePath= os.path.dirname(__file__)
    convertedFilePath= os.path.normpath(filePath)
    return filePath

def cp_fillOutLocations():
    filesDirectory= os.path.normpath(cp_getFilePath() +'/cp_pipelinetool_files')
    iconsDirectory= os.path.normpath(filesDirectory +'/cp_pipelinetool_icons')
    scriptDirectory= os.path.normpath(filesDirectory +'/cp_pipelinetool_scripts')
    return filesDirectory, iconsDirectory, scriptDirectory

def cp_getIcon(iconName= 'commandButton.png'):
    if int(cp_getMayaVersion()) < 2012:
        iconExtension= 'xpm'
    else:
        iconExtension= 'png'

    if str(iconName) == 'commandButton.png':
        newIconName= iconName
    else:
        newIconName= os.path.normpath(cp_fillOutLocations()[1] +'/'+ iconName +'.'+ iconExtension)
    
    return newIconName




def cp_readCommand(scriptName):
    if scriptName != '':
        if '.py' not in scriptName:
            scriptName += '.py'
    scriptPath= os.path.normpath(cp_fillOutLocations()[2] +'/'+ scriptName)

    with open(scriptPath, "r") as scriptContent:
        allLines= scriptContent.readlines()
        scriptContent.close()
        scriptTotal= ''.join(allLines)

    return scriptPath, allLines, scriptTotal

def cp_readCommandSimple(scriptName):
    return cp_readCommand(scriptName)[2]

def cp_exchangePathsInCommand(scriptName, scriptLocation):
    newPathAdded= [line.split('"')[0]+'"'+ scriptLocation +'"'+ line.split('"')[2] if 'newPath=' in line else line for line in cp_readCommand(scriptName)[1]]

    scriptPath= cp_readCommand(scriptName)[0]
    newPathAddedTotal= ''.join(newPathAdded)
    return scriptPath, newPathAdded, newPathAddedTotal

def cp_exchangePathsInInterfaceInScript(scriptName, scriptLocation):
    scriptNameSplit= scriptName.replace('.py','')

    newPathAdded= [line.split('"')[0]+'"'+ scriptLocation +'/'+ scriptNameSplit+ 'UI.ui/"'+ line.split('"')[2] if 'uiFilePath=' in line else line for line in cp_readCommand(scriptName)[1]]

    scriptPath= cp_readCommand(scriptName)[0]
    newPathAddedTotal= ''.join(newPathAdded)
    return scriptPath, newPathAdded, newPathAddedTotal

def cp_changeScriptsInDirectory(scriptName, scriptLocation):
    if scriptName != '':
        if '.py' not in scriptName:
            scriptName += '.py'
    scriptPath= os.path.normpath(cp_fillOutLocations()[2] +'/'+ scriptName)

    with open(scriptPath, "r") as scriptContent:
        allLines= scriptContent.readlines()
        scriptContent.close()
        scriptTotal= ''.join(allLines)
    
    changedScriptPathFile= cp_exchangePathsInCommand(scriptName, scriptLocation)[1]
    scriptTotalNew= ''.join(changedScriptPathFile)

    with open(scriptPath, "w") as scriptContentNew:
        scriptContentNew.write(scriptTotalNew)
        scriptContentNew.close()

    changedScriptPathFile= cp_exchangePathsInInterfaceInScript(scriptName, scriptLocation)[1]
    scriptTotalNew= ''.join(changedScriptPathFile)

    with open(scriptPath, "w") as scriptContentNew:
        scriptContentNew.write(scriptTotalNew)
        scriptContentNew.close()


    return None



def cp_addButton(label, command, icon="commandButton.png"):
    '''Adds a shelf button with the specified label, command, double click command and image.'''
    import maya.mel as mel 

    gShelfTopLevel = mel.eval("$tmpVar=$gShelfTopLevel")
    
    currentShelf= mc.shelfTabLayout( gShelfTopLevel, q= True, selectTab= True)
    mc.setParent(currentShelf)
    mc.shelfButton(width=37, height=37, image=icon, l=label, command=command, imageOverlayLabel=label)