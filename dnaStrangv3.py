#coding: utf8

import maya.cmds as cmds
import math

def createUI():

    if (cmds.window("MainWindow", exists=True)):
            cmds.deleteUI("MainWindow")
            


    ram = cmds.window("MainWindow", t = "UI Ver 1", w = 300, h = 2000)
    cmds.columnLayout(adj = True)
    '''
    die objekte wurden hier bei dir nur in variablen gespeichert, die haben 2 probleme, erstens erreichen sie durch den scope (glaub ich) nicht die funktion zweitens sucht maya nicht in deinem script nach dem Objekt sondern in der szene, da bringen ihm variablen nix; das UI objekt braucht einen Namen
    '''
    createUI_DSlider = cmds.intSliderGrp('jm_UI_DSlider', l = "Dichte", min=1, max=5, value = 1, field = True)
    createUI_PSlider = cmds.intSliderGrp ('jm_UI_PSlider', l = "Anzahl der Paare", min=10, max=100, value=20, field = True)
    createUI_tuSlider = cmds.intSliderGrp ('jm_UI_tuSlider', l = "Rotation", min=0, max=90, value=18, field = True)

    cmds.button( l = "Create Helix", c= "createStrang()")

    cmds.showWindow(ram)
     
     
def createStrang(*args):
    
    # Lösche bestehende Objekte
    if (cmds.objExists("DNA")):
        cmds.delete("DNA")

       
    #Erstellung Basis Geometrie  
    cmds.polySphere(sx=20, sy=20, r=0.5, n='Kugel1')
    cmds.polySphere(sx=20, sy=20, r=0.5, n='Kugel2')
    cmds.move( 4, 0, 0 )
    cmds.polyCylinder( ax= [1,0,0], sx=20, sy=20, sz=5, h=4, r=0.2, n='Zylinder')
    cmds.move( 2, 0, 0 )
    cmds.group ( 'Kugel1', 'Zylinder','Kugel2', n='BaseGC' )
    cmds.move( -2, 0, 0 )
    
    #Abfrage aus Slider
    BasenDichte = cmds.intSliderGrp('jm_UI_DSlider', q= True, value = True)
    punkteZahl = cmds.intSliderGrp('jm_UI_PSlider', q= True, value = True)
    rot = cmds.intSliderGrp('jm_UI_tuSlider', q= True, value = True)

    
    curvePunkte =[(0, 0, 0,)]
    
    #Berechnung Hilfswerte & Float kovertierung
    BasenDichte = BasenDichte * 1.0
    x = punkteZahl * BasenDichte
    
    #Check
    print BasenDichte
    print punkteZahl
    print rot
    print x
    
    #Helix erzeugen durch Duplikation und Rotation
    for i in range(punkteZahl):
        newPunkt = (0, 0, i)
        curvePunkte.append( newPunkt )
 
    cmds.curve( p = curvePunkte)
    cmds.rotate ( oa = [0, 90, 90] )
    grpNameB = cmds.group( em=True, name='Basen' )
    grpNameDNA = cmds.group( em=True, name='DNA' )
    for i in range (i):
        basenZahl = float(i/x)
        
        posBasen = cmds.pointOnCurve( 'curve1', pr= basenZahl, top=True, p=True )
        basenVerteilung = cmds.duplicate( 'BaseGC' )
        cmds.xform(basenVerteilung[0], t=posBasen, r=True)
        cmds.rotate( 0, rot*i, 0, basenVerteilung[0])
        cmds.parent(basenVerteilung[0], grpNameB )
    cmds.parent('curve1', 'BaseGC', grpNameB, grpNameDNA )
    cmds.hide( 'curve1' )
    print grpNameDNA
    