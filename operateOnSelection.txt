import maya.cmds as mc
def set_selection_historicallyNotInteresting():
    for i in mc.ls(sl= True):
        mc.setAttr('{0}.isHistoricallyInteresting'.format(i), 0)
        
    
class OperateOnTransforms():

    def __init__(self, selection):
        self.selection= selection
        
        import maya.cmds as mc


    def return_selectionList(self):
        
        selectedItems= '{0}'.format((', '.join(self.selection)))
        numberOfStringToSpaces= '-'* len(selectedItems)
        
        print ('\n\n' + numberOfStringToSpaces+ '\nthe selected items are: \n\n'+ str(selectedItems) + '\n\n' + numberOfStringToSpaces + '\n\n')
        return (selectedItems)
        

    def return_selectionTransforms(self):
        
        selectedItemTransformations= []
        
        for i in self.selection:
            position= tuple(mc.xform(i, q= True, t= True))
            
            selectedItemTransformations.append(position)
        
        numberOfStringToSpaces= '-'* len(str(selectedItemTransformations))
        print ('\n\n{0}\nthe selected items are: \n\n'+ str(selectedItemTransformations) + '\n\n{0}\n\n'.format(numberOfStringToSpaces))
        return selectedItemTransformations
'''
    def create_curve_from_Transform(self):
        for i in return_selectionTransforms()
            curvePoint= tuple(i)
            print curvePoint
        
'''
 
 
        
OperateOnTransforms(mc.ls(sl= True)).return_selectionList()

OperateOnTransforms(mc.ls(sl= True)).return_selectionTransforms()