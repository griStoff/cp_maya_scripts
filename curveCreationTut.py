import maya.cmds as mc

def main(length= 40, distance=20, taper= 1):
	import math
	
	tapering= 1.0000/length
	taperMan= 1
	
	
	pointList= []
	for i in range(length):
		sinValue= (math.sin(i))*(distance*taperMan)
		gesValue= (0,i,sinValue)
		pointList.append(gesValue)
		
		taperMan -= tapering
	

		
	mc.curve(n='hubbaHubba', p= pointList)

	taperMan= 1
	pointList= []
	for i in range(length):
		sinValue= (math.sin(i*(-1)))*(distance*taperMan)
		gesValue= (sinValue,i,0)
		pointList.append(gesValue)
		
		taperMan -= tapering
	

		
	mc.curve(n='bubbaBubba', p= pointList)