def create_three_joint_setup():
    import maya.cmds as mc
    import maya.OpenMaya as om
    
    inputJoints= mc.ls(sl= True)
    
    yo= triangle_vector_ops(inputJoints)
    fistAndLastPoint= yo[0][0] , yo[0][2]
    fistAndLastPoint= list(fistAndLastPoint)
    
    
    resultCurve= mc.curve(name='curve1', p= fistAndLastPoint, degree= 1)
    mc.rebuildCurve(rt=0, s=30)
    
    
    cp_startJoint= mc.joint(p= yo[0][0], radius= .3)
    mc.select(clear= True)
    
    cp_endJoint= mc.joint(p= yo[0][2], radius= .3)
    mc.select(clear= True)
    
    cp_middleJoint= mc.joint(p= yo[0][2], radius= .3)
    mc.select(clear= True)
    
    cp_constraint= proportionalWeight_parentConstraint(inputJoints, cp_middleJoint)[0]
    #mc.delete(cp_constraint)
    
    
    mc.select(cp_endJoint, cp_middleJoint, cp_startJoint, resultCurve)
    mc.skinCluster(tsb=True)


def weight_curve_skin_proportional(inputCurve, inputParameter= ()):
    import maya.cmds as mc
        
    targetCurve = inputCurve
    curveCvs = mc.ls('{0}.cv[:]'.format(targetCurve), fl = True)
    
    if inputParameter != ():
        print 'yay'


        firstHalf=[]
        secondHalf=[]
        for position, i in enumerate( curveCvs):
            currentValue= position*(1.0/len(curveCvs))
        
            if currentValue <= inputParameter:
                firstHalf.append(i)
            else:
                secondHalf.append(i)

    else:
        print 'nay'

        halvedList= (len( curveCvs)/2)
        
        firstHalf= curveCvs[:halvedList]
        firstHalf.reverse()
        print firstHalf
        
        secondHalf= curveCvs[halvedList:]
        print secondHalf
        
        halvedCvs = 1/(float(( len( curveCvs)/2)))
    
    if curveCvs:
        '''
        for cv in curveCvs:
        '''
        for position, cv in enumerate (curveCvs):
            mc.skinPercent( 'skinCluster2', cv, transformValue=[('joint3', 1)])
        
        for position, cv in enumerate (secondHalf):
            mc.skinPercent( 'skinCluster2', cv, transformValue=[('joint2', position* halvedCvs)])
        
        for position, cv in enumerate (firstHalf):    
            mc.skinPercent( 'skinCluster2', cv, transformValue=[('joint1', position* halvedCvs)])
    
    else:
        cmds.warning('Found no cvs!')
        
    print (mc.skinCluster('skinCluster2',query=True,inf=True))


def get_curve_param_on_pos():
    temp_jointPos= mc.xform('joint3', ws= True, t=True, q= True)
    
    npC = mc.createNode("nearestPointOnCurve")
    mc.connectAttr("curve1.worldSpace", npC + ".inputCurve")
    mc.setAttr(npC + ".inPosition", temp_jointPos[0], temp_jointPos[1], temp_jointPos[2]) 
    
    parameterValue= cmds.getAttr(npC + ".parameter")
    mc.delete(npC)
    
    return parameterValue


print (get_curve_param_on_pos())
weight_curve_skin_proportional('curve1', get_curve_param_on_pos())

