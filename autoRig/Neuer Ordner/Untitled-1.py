import maya.cmds as mc
import maya.OpenMaya as om

def cp_normalizeCurveWeights():
    allSelected= mc.ls(sl= True, fl= True)
    allSelectedLength= len(allSelected)
    imMorts= ['Cr_mouthLower_RIG', 'Cr_mouthUpper_RIG', 'Rt_mouthCorner_RIG', 'Lf_mouthCorner_RIG']
    selectedSkinCluster= 'skinCluster31'


    if allSelectedLength != 0:
        if allSelectedLength % 2:
            
            listHalved= (allSelectedLength-1)/2
            percentagePoints= 1.0/ listHalved
            
            firstSelection= allSelected[:listHalved]
            secondSelection= allSelected[listHalved:listHalved+1]
            thirdSelection= reversed(allSelected[listHalved + 1:])
            
            
            
            startingPoint= 0
            invertedPoint= 1- startingPoint
            for i in firstSelection:
                mc.skinPercent( selectedSkinCluster, i, transformValue=[('Rt_mouthCorner_RIG', invertedPoint), ('Cr_mouthLower_RIG', startingPoint)])
                
                print startingPoint
                startingPoint += percentagePoints
                invertedPoint = 1- startingPoint



            for i in secondSelection:
                mc.skinPercent( selectedSkinCluster, i, transformValue=[('Cr_mouthLower_RIG', 1)])
                



            startingPoint= 0
            invertedPoint= 1-invertedPoint
            for i in thirdSelection:
                mc.skinPercent( selectedSkinCluster, i, transformValue=[('Lf_mouthCorner_RIG', invertedPoint), ('Cr_mouthLower_RIG', startingPoint)])
                
                startingPoint += percentagePoints
                invertedPoint = 1- startingPoint

        else:
            print 'nay'
    else:
        print 'nay'

def main():
    import sys
    import os.path
    import maya.cmds as mc
    
    uiFilePath= os.path.normpath("C:\\work\\cp_maya_scripts\\cp_maya_scripts\\objectsOnCurve\\cp_objectsOnCurve.ui")
    if mc.window('cp_objectsOnCurveWindow', exists= True):
        mc.deleteUI('cp_objectsOnCurveWindow')
    
    window= mc.loadUI(uiFile= uiFilePath)
    mc.showWindow(window)