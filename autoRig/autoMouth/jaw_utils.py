import maya.cmds as mc



# object constants ################################################################################################

cp_GROUP= 'grp'
cp_JOINT= 'jnt'
cp_GUIDE= 'guide'
cp_OBJECT= 'jaw'
cp_RIG= 'rig'
cp_OFFSET= 'srtOffset'

cp_JOINTSCALE= 3
cp_MINORJOINTSCALE= cp_JOINTSCALE* 0.3



# side constants

cp_SIDES= {'LEFT': 'Lf', 'RIGHT':'Rt', 'CENTER': 'Cr', 'CORNER': 'Corner'}
seperationList= ['Upper','Lower']

# operations GET ##################################################################################################

def cp_get_lipGuides():
    """


    :param number:
    :return:
    """
    grp= '{side}_{object}_lipMinor_{name}_{suffix}'.format(side= cp_SIDES['CENTER'], object= cp_OBJECT, name= cp_GUIDE, suffix= cp_GROUP)
    return [loc for loc in mc.listRelatives(grp) if mc.objExists(grp)]

def cp_get_specialGuides():
    return [loc for loc in cp_get_lipGuides() if cp_SIDES['CENTER'] in loc or cp_SIDES['CORNER'] in loc]

def cp_get_jawGuides():
    """


    :param number:
    :return:
    """
 
 
    grp= '{side}_{object}_base_{name}_{suffix}'.format(side= cp_SIDES['CENTER'], object= cp_OBJECT, name= cp_GUIDE, suffix= cp_GROUP)
    return [loc for loc in mc.listRelatives(grp) if mc.objExists(grp)]


def cp_getLip_Parts():
    
    broadJointsList= []
    for guide in cp_get_specialGuides():

        jointName= guide.replace(cp_GUIDE, cp_JOINT)
        broadName= jointName.replace('lip','lipBroad')
        broadJointsList.append(broadName)
    
    print broadJointsList
    '''
    upper_token= 'jawUpper'
    lower_token= 'jawLower'
    corner_token= 'jawCorner'

    C_upper= broadJointsList[0]
    C_lower= broadJointsList[1]
    L_corner= broadJointsList[2]
    R_corner= broadJointsList[3]

    lipJointList= mc.listRelatives()
    print cp_get_lipGuides()
    '''
# Operations General ################################################################################################

def cp_build():
    cp_createHierarchy()
    cp_createMinorJoints()
    cp_createBroadJoints()
    jawJoints= cp_createJawJoints()
    for i in jawJoints:
        cp_addOffset(i)

    cp_constraintBroadJoints()
    cp_getLip_Parts()
# Create Offset Group ###############################################################################################

def cp_addOffset(inNode, suffix= cp_OFFSET):
    """



    :return:
    """
    grpOffset= mc.createNode('transform', name= inNode.replace(cp_JOINT, cp_JOINT+ '_'+ cp_OFFSET))

    offsetMatrix= mc.xform(inNode, query= True, matrix= True, ws= True)
    mc.xform(grpOffset, m= offsetMatrix, ws= True)

    inNodeParent= mc.listRelatives(inNode, parent= True)
    if inNodeParent:
        mc.parent(grpOffset, inNodeParent)
    
    mc.parent(inNode, grpOffset)
    
    return grpOffset

# Create the guides #################################################################################################

def cp_createGuides(number= 5):
    """


    :param number:
    :return:
    """

    zFillLength= len(str(number)) +1


    guidesList= []

    jaw_guide_grp= mc.createNode('transform', name= '{side}_{object}_{name}_{suffix}'.format(side= cp_SIDES['CENTER'], object= cp_OBJECT, name= cp_GUIDE, suffix= cp_GROUP))
    locs_guide_grp= mc.createNode('transform', name= '{side}_{object}_lip_{name}_{suffix}'.format(side= cp_SIDES['CENTER'], object= cp_OBJECT, name= cp_GUIDE, suffix= cp_GROUP), parent= jaw_guide_grp)
    lip_locs_guide_grp= mc.createNode('transform', name= '{side}_{object}_lipMinor_{name}_{suffix}'.format(side= cp_SIDES['CENTER'], object= cp_OBJECT, name= cp_GUIDE, suffix= cp_GROUP), parent= locs_guide_grp)
    jawBase_guide_grp= mc.createNode('transform', name= '{side}_{object}_base_{name}_{suffix}'.format(side= cp_SIDES['CENTER'], object= cp_OBJECT, name= cp_GUIDE, suffix= cp_GROUP), parent= jaw_guide_grp)


    leftAndRightSide= [cp_SIDES['LEFT'], cp_SIDES['RIGHT']]
    for part in seperationList:
        #CREATE CENTERS
        part_mult= 1 if part == 'Upper' else -1
        midData= (0, part_mult, 0)

        mid_loc= mc.spaceLocator(name= '{side}_{object}{part}_lip_{suffix}'.format(side= cp_SIDES['CENTER'], object= cp_OBJECT, part= part, suffix= cp_GUIDE))[0]
        guidesList.append(mid_loc)
        mc.parent(mid_loc, lip_locs_guide_grp)

        mc.setAttr('{0}.t'.format(mid_loc), *midData)

        #CREATE MIDSECTION
        for side in leftAndRightSide:
            for x in range(number):
                multiplier = x+1 if side == leftAndRightSide[0] else -(x+1)
                
                locData= (multiplier, part_mult, 0)
                
                locatorBySide= mc.spaceLocator(name= '{side}_{object}{part}_lip_{numberation}_{suffix}'.format(side= side, object= cp_OBJECT, part= part, numberation= str((x+1)).zfill(zFillLength), suffix= cp_GUIDE))[0]
                mc.parent(locatorBySide, lip_locs_guide_grp)

                mc.setAttr('{0}.t'.format(locatorBySide), *locData)
                guidesList.append(locatorBySide)

    #CREATE CORNERS

    leftCornerLoc= mc.spaceLocator(name= '{side}_{object}Corner_lip_{suffix}'.format(side= cp_SIDES['LEFT'], object= cp_OBJECT, suffix= cp_GUIDE))[0]
    rightCornerLoc= mc.spaceLocator(name= '{side}_{object}Corner_lip_{suffix}'.format(side= cp_SIDES['RIGHT'], object= cp_OBJECT, suffix= cp_GUIDE))[0]
   
    
    mc.parent(leftCornerLoc, lip_locs_guide_grp)
    mc.parent(rightCornerLoc, lip_locs_guide_grp)

    mc.setAttr('{0}.t'.format(leftCornerLoc), *(number + 1, 0,0))
    mc.setAttr('{0}.t'.format(rightCornerLoc), *(-(number + 1), 0,0))
    
    guidesList.append(leftCornerLoc)
    guidesList.append(rightCornerLoc)


    mc.select(cl= True)

    #CREATE JAW

    
    jawBaseLoc= mc.spaceLocator(name= '{side}_{object}_{suffix}'.format(side= cp_SIDES['CENTER'], object= cp_OBJECT, suffix= cp_GUIDE))[0]
    
    jawBaseInverseLoc= mc.spaceLocator(name= '{side}_{object}_inverse_{suffix}'.format(side= cp_SIDES['CENTER'], object= cp_OBJECT, suffix= cp_GUIDE))[0]

    mc.setAttr('{}.t'.format(jawBaseLoc), *(0, -1, -number))
    mc.setAttr('{}.t'.format(jawBaseInverseLoc), *(0, 1, -number))

    mc.parent(jawBaseLoc, jawBase_guide_grp)
    mc.parent(jawBaseInverseLoc, jawBase_guide_grp)

    guidesList.append(jawBaseLoc)
    guidesList.append(jawBaseInverseLoc)

    mc.select(cl= True)
    print(guidesList)

# Create Joints

def cp_createHierarchy():
    """


    :return:
    """
    
    mainJoint_grp= mc.createNode('transform', name= '{side}_{object}_{name}_{suffix}'.format(side= cp_SIDES['CENTER'], object= cp_OBJECT, name= cp_RIG, suffix= cp_GROUP))
    lipJoint_grp= mc.createNode('transform', name= '{side}_{object}_lip_{name}_{suffix}'.format(side= cp_SIDES['CENTER'], object= cp_OBJECT, name= cp_RIG, suffix= cp_GROUP), parent= mainJoint_grp)
    baseJoint_grp= mc.createNode('transform', name= '{side}_{object}_base_{name}_{suffix}'.format(side= cp_SIDES['CENTER'], object= cp_OBJECT, name= cp_RIG, suffix= cp_GROUP), parent= mainJoint_grp)

    lipMinorJoint_grp= mc.createNode('transform', name= '{side}_{object}_lipMinor_{name}_{suffix}'.format(side= cp_SIDES['CENTER'], object= cp_OBJECT, name= cp_RIG, suffix= cp_GROUP), parent= lipJoint_grp)
    lipBroadJoint_grp= mc.createNode('transform', name= '{side}_{object}_lipBroad_{name}_{suffix}'.format(side= cp_SIDES['CENTER'], object= cp_OBJECT, name= cp_RIG, suffix= cp_GROUP), parent= lipJoint_grp)

    mc.select(clear= True)

def cp_createMinorJoints():
    """


    :return:
    """

    minorJointsList= list()
    for guide in cp_get_lipGuides():

        jointName= guide.replace(cp_GUIDE, cp_JOINT)

        matrixInfo= mc.xform (guide, query= True, matrix= True, ws= True)
        
        jnt= mc.joint(name= jointName)

        mc.setAttr('{name}.radius'.format(name=jnt), cp_MINORJOINTSCALE)
        mc.xform(jnt, matrix= matrixInfo, ws= True)

        mc.parent(jnt, '{side}_{object}_lipMinor_{name}_{suffix}'.format(side= cp_SIDES['CENTER'], object= cp_OBJECT, name= cp_RIG, suffix= cp_GROUP))
        
        
        minorJointsList.append(jnt)
    
    return minorJointsList



def cp_createBroadJoints():
    """


    :return:
    """
    broadJointsList= []

    for guide in cp_get_specialGuides():

        jointName= guide.replace(cp_GUIDE, cp_JOINT)
        broadName= jointName.replace('lip','lipBroad')

        matrixInfo= mc.xform (guide, query= True, matrix= True, ws= True)
        
        jnt= mc.joint(name= broadName)

        mc.setAttr('{name}.radius'.format(name=jnt), cp_JOINTSCALE)
        mc.xform(jnt, matrix= matrixInfo, ws= True)

        mc.parent(jnt, '{side}_{object}_lipBroad_{name}_{suffix}'.format(side= cp_SIDES['CENTER'], object= cp_OBJECT, name= cp_RIG, suffix= cp_GROUP))
        
        broadJointsList.append(jnt)
    
    return broadJointsList


def cp_createJawJoints():
    """


    :return:
    """
    jawJointsList= []
    
    for guide in cp_get_jawGuides():

        jointName= guide.replace(cp_GUIDE, cp_JOINT)

        matrixInfo= mc.xform (guide, query= True, matrix= True, ws= True)
        
        jnt= mc.joint(name= jointName)

        mc.setAttr('{name}.radius'.format(name=jnt), cp_JOINTSCALE)
        mc.xform(jnt, matrix= matrixInfo, ws= True)

        mc.parent(jnt, '{side}_{object}_base_{name}_{suffix}'.format(side= cp_SIDES['CENTER'], object= cp_OBJECT, name= cp_RIG, suffix= cp_GROUP))
        
        jawJointsList.append(jnt)
    
    return jawJointsList
    
def cp_constraintBroadJoints():
    """


    :return:
    """

    jawJoints= [i.replace(cp_GUIDE, cp_JOINT) for i in cp_get_jawGuides()]
    jawJoints= [i.replace('lip','lipBroad') for i in jawJoints]

    broadJoints= [i.replace(cp_GUIDE, cp_JOINT) for i in cp_get_specialGuides()]
    broadJoints= [i.replace('lip','lipBroad') for i in broadJoints]

    offsetList= []
    for broadJoint in broadJoints:
        broadOffset= cp_addOffset(broadJoint)
        offsetList.append(broadOffset)
    
    print offsetList
    print jawJoints

    mc.parentConstraint (jawJoints[1], offsetList[0], mo= True)
    mc.parentConstraint (jawJoints[0], offsetList[1], mo= True)

    mc.parentConstraint (broadJoints[0], broadJoints[1], offsetList[2], mo= True)
    mc.parentConstraint (broadJoints[0], broadJoints[1], offsetList[3], mo= True)

    mc.select(clear= True)

