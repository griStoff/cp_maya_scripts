
import maya.cmds as mc

import sys
import os.path

newPath= os.path.normpath("C:\\work\\cp_maya_scripts\\cp_maya_scripts\\autoRig\\autoMouth")

for module_name in sys.modules.keys():
    topModule= module_name.split('.')[0]
    if topModule == 'autoMouth':
        del (sys.modules[module_name])

if not newPath in sys.path:
    sys.path.append(newPath)
    
import jaw_utils
reload (jaw_utils)


jaw_utils.cp_createGuides()

jaw_utils.cp_build()