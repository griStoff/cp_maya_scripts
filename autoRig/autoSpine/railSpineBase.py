def cp_railSpline(inputList):
    import maya.cmds as mc
    for i in inputList:
        decomposedName= str(i).split('_')
        baseName= '_'.join(decomposedName[:-1])
        axisList= ['x', 'y', 'z']
        
        
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~ nodeCreation
        inTransDecomposeMatrix_node = mc.createNode('decomposeMatrix', n= '{0}_inTrans_dMtx'.format(baseName))
        
        closestPointOnSurface_node= mc.createNode('closestPointOnSurface', n= '{0}_cPS'.format(baseName))
        
        pointOnSurfaceInfo_node= mc.createNode('pointOnSurfaceInfo', n= '{0}_pSi'.format(baseName))
        mc.setAttr('{0}.parameterV'.format(pointOnSurfaceInfo_node), 0.5)
        
        vectorProduct_node= mc.createNode('vectorProduct', n= '{0}_vpr'.format(baseName))
        mc.setAttr('{0}.operation'.format(vectorProduct_node), 2)
        
        invertXvector_node= mc.createNode('multiplyDivide', n= '{0}_mult'.format(baseName))
        
        for pos, axis in enumerate(axisList):
            mc.setAttr('{0}.input1{1}'.format(invertXvector_node, axis.capitalize()), -1)
        
        fourByFourMatrix_node= mc.createNode('fourByFourMatrix', n= '{0}_fbf'.format(baseName))
        
        outTransDecomposeMatrix_node= mc.createNode('decomposeMatrix', n= '{0}_outTrans_dMtx'.format(baseName))
        
        transform_node= mc.createNode('transform', n= '{0}_pos'.format(baseName))
        
        
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~ nodeConnection
        
        
        mc.connectAttr('{0}.worldMatrix[0]'.format(i), '{0}.inputMatrix'.format(inTransDecomposeMatrix_node))


        for pos, axis in enumerate(axisList):
            mc.connectAttr('{0}.outputTranslate{1}'.format(inTransDecomposeMatrix_node, axis.capitalize()), '{0}.inPosition{1}'.format(closestPointOnSurface_node, axis.capitalize()))
            
            mc.connectAttr('{0}.outputTranslate{1}'.format(outTransDecomposeMatrix_node, axis.capitalize()), '{0}.translate{1}'.format(transform_node, axis.capitalize()))
            
            mc.connectAttr('{0}.outputRotate{1}'.format(outTransDecomposeMatrix_node, axis.capitalize()), '{0}.rotate{1}'.format(transform_node, axis.capitalize()))

            mc.connectAttr('{0}.normal{1}'.format(pointOnSurfaceInfo_node, axis.capitalize()), '{0}.input1{1}'.format(vectorProduct_node, axis.capitalize()))
            
            mc.connectAttr('{0}.normalizedTangentU{1}'.format(pointOnSurfaceInfo_node, axis.capitalize()), '{0}.input2{1}'.format(vectorProduct_node, axis.capitalize()))
        
            mc.connectAttr('{0}.normalizedTangentU{1}'.format(pointOnSurfaceInfo_node, axis.capitalize()), '{0}.input2{1}'.format(invertXvector_node, axis.capitalize()))
            
            
            
            mc.connectAttr('{0}.normalizedNormal{1}'.format(pointOnSurfaceInfo_node, axis.capitalize()), '{0}.in1{1}'.format(fourByFourMatrix_node, pos))
        
            mc.connectAttr('{0}.output{1}'.format(invertXvector_node, axis.capitalize()), '{0}.in0{1}'.format(fourByFourMatrix_node, pos))
            
            mc.connectAttr('{0}.output{1}'.format(vectorProduct_node, axis.capitalize()), '{0}.in2{1}'.format(fourByFourMatrix_node, pos))
            
            mc.connectAttr('{0}.position{1}'.format(pointOnSurfaceInfo_node, axis.capitalize()), '{0}.in3{1}'.format(fourByFourMatrix_node, pos))





        mc.connectAttr('{0}.parameterU'.format(closestPointOnSurface_node), '{0}.parameterU'.format(pointOnSurfaceInfo_node))
        
        mc.connectAttr('{0}.output'.format(fourByFourMatrix_node), '{0}.inputMatrix'.format(outTransDecomposeMatrix_node))
 
        
cp_railSpline(mc.ls(sl= True))