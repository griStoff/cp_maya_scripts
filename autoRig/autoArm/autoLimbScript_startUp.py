import sys
import os.path

newPath= os.path.normpath("C:\\work\\cp_maya_scripts\\cp_maya_scripts\\autoRig\\autoArm\\")

if not newPath in sys.path:
    sys.path.append(newPath)
    
import autoLimbScript
reload (autoLimbScript)

autoLimbScript.executeRig('arm', inputSubD= 5)