############################################################################################
############################################################################################
###                                                                                      ###
###                                autolimb script 0.24                                  ###
###                                 christof pehhringer                                  ###
###                                                                                      ###
############################################################################################
############################################################################################


#########################################################################################################################################
#
# 
# to DO: implement aim rig for fk; implement spaces; ----->bendyArm
#
#
#
#########################################################################################################################################

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ load libraries

import maya.cmds as mc
import maya.OpenMaya as om
import maya.mel as mel
import json as js
import sys
import os.path


#~~~~~~~~~~~~~~~~~~~~~~~~~~ load plugins required

#~~~~~~~~~~~~~~~~~~~~~~~~~~ maya internal plugins
cp_versionCheck= mc.about(v=True)
cp_pluginPath= 'C:/Program Files/Autodesk/Maya{0}/bin/plug-ins/'.format(str(cp_versionCheck))

#~~~~~~~~~~~~~~~~~~~~~~~~~~ load plugins required
mc.loadPlugin( '{0}quatNodes.mll'.format(cp_pluginPath), quiet = True)
mc.loadPlugin( '{0}matrixNodes.mll'.format(cp_pluginPath), quiet = True)




#~~~~~~~~~~~~~~~~~~~~~~~~~~ maya external skripts


newPath= os.path.normpath("C:\\work\\cp_maya_scripts\\cp_maya_scripts")

if not newPath in sys.path:
    sys.path.append(newPath)
    print 'added new path'

standardOpsPath= os.path.normpath("C:\\work\\cp_maya_scripts\\cp_maya_scripts\\standardOps\\")

if not standardOpsPath in sys.path:
    sys.path.append(standardOpsPath)

curveToolsPath= os.path.normpath("C:\\work\\cp_maya_scripts\\cp_maya_scripts\\standardOps\\curveTools\\")

if not curveToolsPath in sys.path:
    sys.path.append(curveToolsPath)



#~~~~~~~~~~~~~~~~~~~~~~~~~~ load scripts required

import curveCreationTut
reload(curveCreationTut)

import cp_curveTools
reload (cp_curveTools)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ define curves


diamondCurve= cp_curveTools.cp_loadJsonCurve("diamond_crv", "C:\\work\\cp_maya_scripts\\setup\\curves")[1]

ikCurve= cp_curveTools.cp_loadJsonCurve("cube_crv", "C:\\work\\cp_maya_scripts\\setup\\curves")[1]
 
fkCurve= cp_curveTools.cp_loadJsonCurve("dentCircle_crv", "C:\\work\\cp_maya_scripts\\setup\\curves")[1]

crossCurve= cp_curveTools.cp_loadJsonCurve("cross_crv", "C:\\work\\cp_maya_scripts\\setup\\curves")[1]



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ seperate Prefixes
cp_allowedPrefixes= ['Lf_', 'L_', 'Rt_', 'R_', 'Cr_', 'C_']


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ dictionaries
dictBodyParts = {'arm': ['shoulder', 'humerus', 'elbow', 'ulnar', 'wrist'], 'leg': ['hip', 'femur', 'knee', 'tibia', 'ankle'], 'undefined': ['helper01', 'helper02', 'helper03', 'helper04', 'helper05', 'helper06', 'helper07', 'helper08']}
dictColorValues = {'left':18, 'right': 20, 'center': 22}


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ sort out Joints


jointList= mc.ls(sl= True, type= 'joint')

allButLastJointsList= jointList[:-1]
allButFirstJointsList= jointList[1:]


#######################################################
#############    rename functions    ##################
#######################################################

def format_object_name(renamedObject, endString, insertString= ''):
    """ input: 2 mandatory 1 additional for inserts // formats strings based on the specified endstring, and the insertString """
    
    prefix, middle = renamedObject.split('_', 1)
    middle, suffix = middle.rsplit('_', 1)
    
    if insertString != '':
        middle = middle.capitalize()
    
    newName = '{side}_{type}{name}_{suffix}'.format(side= prefix, type= insertString, name= middle, suffix= endString)
    
    return newName

## reformats list from ik to fk etc

def format_list_name(listType, endString, insertString=''):
    """ input: 2 mandatory 1 additional for inserts // takes rename_object function and renames the input list """
    
    joints= mc.ls(sl= True, type= listType)
    
    ikNames =[]
    
    for i in joints:
                
        ikNames.append(format_object_name(i, endString, insertString))
        
    return ikNames

## create Names based on list, reformat the middle

def change_strings_by_list(times, inputString, insertString):
    """ documentation """
    oldString= str(inputString)
    newString= insertString
    
    disectedString= oldString.split('_')
    
    renamedNames= []
    for i in range(0,times):
        if times > 1:
            insertNewString= newString +'_0'+ str(i+1)
        else:
            insertNewString= newString

        renamedNames.append('{first}_{second}_{third}'.format(first= disectedString[0],second= insertNewString,third= disectedString[2]))
    return renamedNames


#################################################
############  PERFORM OPERATIONS  ###############
#################################################
def get_world_matrix(inputList):
    worldMatrixList=[]
    
    for i in inputList:
        worldMatrix= mc.xform(i,m= True, q= True, ws= True)
        worldMatrixList.append(worldMatrix)
        worldMatrixTuple= tuple(worldMatrixList)
    return worldMatrixList, worldMatrixTuple
    
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ add MasterNode
def create_master_node(name, inputList, limb= 'arm'):
    """ input: ; output: nodeName, connectedControlsAttr,  """
    
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ input Data
    nodeName= name
    limbName= limb.capitalize()
    inputData= triangle_vector_ops(inputList)
    
    masterNode= cp_createNodeWithoutHistory('transform', n= nodeName)
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ remove clutter
    axesList= ['x','y','z']
    transformList= ['t', 'r', 's']
    
    for axes in axesList:
        for transform in transformList:
            mc.setAttr('{node}.{operation}{ending}'.format(node= nodeName, operation= transform, ending= axes), lock= True, keyable= False, channelBox= False)
    
    mc.setAttr('{node}.visibility'.format(node= nodeName), lock= True, keyable= False, channelBox= False)
    
    
    #~~~~~~~~~~~~~~~~~~~~~~ add custom attributes

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~ rig Information
    rigScaling= mc.addAttr(masterNode, longName= 'rigScale', attributeType= 'double', defaultValue= 1, hasSoftMinValue= True, softMinValue=0.001)
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~ rig Information
    
    
    #~~~~~~~~~~~~~~~~~~~~~~~ controls Information
    controlsInfo= mc.addAttr(masterNode,longName= 'controlsInfo', numberOfChildren=2, attributeType='compound')
    
    mc.addAttr(masterNode, longName= 'connectedControls', attributeType= 'message', parent= 'controlsInfo')
    mc.addAttr( longName= 'additionalInfo', attributeType= 'message', parent= 'controlsInfo')
    
    
    connectedControlsAttr= '{name}.connectedControls'.format(name= masterNode)
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~ joint Information
    
    upperArmLength= mc.addAttr(masterNode, longName= 'upper{0}Length'.format(limbName), attributeType= 'double')
    mc.setAttr('{0}.upper{1}Length'.format(masterNode, limbName), inputData[2][0], lock= True)

    lowerArmLength= mc.addAttr(masterNode, longName= 'lower{0}Length'.format(limbName), attributeType= 'double')
    mc.setAttr('{0}.lower{1}Length'.format(masterNode, limbName), inputData[2][1], lock= True)

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    shoulderInformation= mc.addAttr(masterNode, longName= 'shoulderInfo', numberOfChildren=3, attributeType='compound')
    shoulderIkJointAttrName='shoulderIkJnts'
    shoulderFkJointAttrName='shoulderFkJnts'
    shoulderOutJointAttrName= 'shoulderOutJnts'
    
    shoulderIkJoints= mc.addAttr(masterNode, longName= shoulderIkJointAttrName, attributeType= 'message', parent= 'shoulderInfo')
    shoulderFkJoints= mc.addAttr(masterNode, longName= shoulderFkJointAttrName, attributeType= 'message', parent= 'shoulderInfo')
    shoulderOutJoints= mc.addAttr(masterNode, longName= shoulderOutJointAttrName, attributeType= 'message', parent= 'shoulderInfo')
    
    shoulderConnectedJointAttrList= [nodeName+'.'+shoulderIkJointAttrName, nodeName+'.'+shoulderFkJointAttrName, nodeName+'.'+shoulderOutJointAttrName]


    elbowInformation= mc.addAttr(masterNode, longName= 'elbowInfo', numberOfChildren=3, attributeType='compound')
    elbowIkJointAttrName='elbowArmIkJnts'
    elbowFkJointAttrName='elbowArmFkJnts'
    elbowOutJointAttrName= 'elbowArmOutJnts'
    
    elbowIkJoint= mc.addAttr(masterNode, longName= elbowIkJointAttrName, attributeType= 'message', parent= 'elbowInfo')
    elbowFkJoint= mc.addAttr(masterNode, longName= elbowFkJointAttrName, attributeType= 'message', parent= 'elbowInfo')
    elbowOutJoints= mc.addAttr(masterNode, longName= elbowOutJointAttrName, attributeType= 'message', parent= 'elbowInfo')
    
    elbowConnectedJointAttrList= [masterNode +'.'+elbowIkJointAttrName, masterNode +'.'+elbowFkJointAttrName, masterNode +'.'+elbowOutJointAttrName]


    wristInformation= mc.addAttr(masterNode, longName= 'wristInfo', numberOfChildren=3, attributeType='compound')
    wristIkJointAttrName='wristArmIkJnts'
    wristFkJointAttrName='wristArmFkJnts'
    wristOutJointAttrName= 'wristArmOutJnts'
    
    wristIkJoint= mc.addAttr(masterNode, longName= wristIkJointAttrName, attributeType= 'message', parent= 'wristInfo')
    wristFkJoint= mc.addAttr(masterNode, longName= wristFkJointAttrName, attributeType= 'message', parent= 'wristInfo')
    wristOutJoints= mc.addAttr(masterNode, longName= wristOutJointAttrName, attributeType= 'message', parent= 'wristInfo')
    
    wristConnectedJointAttrList= [masterNode +'.'+wristIkJointAttrName, masterNode +'.'+wristFkJointAttrName, masterNode +'.'+wristOutJointAttrName]
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    #~~~~~~~~~~~~~~~~ matrix_position_information
    attributesList=[]
    matrixInformation= get_world_matrix(inputList)[1]
    lowerArm= mc.addAttr(masterNode, longName= 'matrixInfo', numberOfChildren=3, attributeType='compound')
    
    shoulderMatrix= mc.addAttr(masterNode, longName= 'shoulderMatrix', attributeType= 'matrix', parent= 'matrixInfo')
    shoulderMatrixName= '{0}.{1}'.format(masterNode, 'shoulderMatrix')
    attributesList.append(shoulderMatrixName)
    
    elbowMatrix= mc.addAttr(masterNode, longName= 'elbowMatrix', attributeType= 'matrix', parent= 'matrixInfo')
    elbowMatrixName= '{0}.{1}'.format(masterNode, 'shoulderMatrix')
    attributesList.append(elbowMatrixName)
    
    wristMatrix= mc.addAttr(masterNode, longName= 'wristMatrix', attributeType= 'matrix', parent= 'matrixInfo')
    wristMatrixName= '{0}.{1}'.format(masterNode, 'shoulderMatrix')
    attributesList.append(wristMatrixName)
    
    mc.setAttr('{0}.shoulderMatrix'.format(masterNode), matrixInformation[0], type= 'matrix', lock= True)
    mc.setAttr('{0}.elbowMatrix'.format(masterNode), matrixInformation[1], type= 'matrix', lock= True)
    mc.setAttr('{0}.wristMatrix'.format(masterNode), matrixInformation[2], type= 'matrix', lock= True)
    
    compoundedConnectedAttrList= [shoulderConnectedJointAttrList, elbowConnectedJointAttrList, wristConnectedJointAttrList]
    
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~ return Attributes
    return masterNode, connectedControlsAttr, attributesList, compoundedConnectedAttrList




####################################################################################################################################################################################################
####################################################################################################################################################################################################

def create_custom_pickwalk(masterInput, listInput):
    print input
    tagNameList= []
    
    masterNode= cp_createNodeWithoutHistory('controller', n='{name}_tag'.format(name= masterInput))
    
    for i in listInput:
        
        tagName= cp_createNodeWithoutHistory('controller', n='{name}_tag'.format(name= i))
        mc.connectAttr('{name}.message'.format(name= i), '{name}.controllerObject'.format(name= tagName))
        tagNameList.append(tagName)
            
    for position, item in enumerate(tagNameList):
         mc.connectAttr('{name}.parent'.format(name= item), '{name}.children[{index}]'.format(name= tagNameList[position+1], index= position))
         mc.connectAttr('{name}.prepopulate'.format(name= item), '{name}.prepopulate'.format(name= tagNameList[position+1]))
    
    lastTagItem= tagNameList[-1]
    mc.connectAttr('{name}.children[0]'.format(name= lastTagItem), '{name}.parent'.format(name= masterNode))     

####################################################################################################################################################################################################
####################################################################################################################################################################################################

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ constraints

def create_pv_constraint(pvControl, handle, inputName):
    """ inputs: aimObject, ikHandle; outputs: """
    
    pvC= mc.poleVectorConstraint( pvControl, handle, n= inputName)
    mc.hide(pvC)
    return pvC

def create_point_constraint(master, slave, inputName, weight= 1):
    """  inputs: master, slave; outputs:  """
    
    if weight is 1:
        ptC= mc.pointConstraint(master, slave, n= inputName)
    else:
        ptC= mc.pointConstraint(master, slave, n= inputName, w= weight)
    mc.hide(ptC)
    return ptC

def create_orient_constraint(master, slave, inputName, weight= 1):
    """  inputs: master, slave; outputs:  """
    
    if weight is 1:
        orC= mc.orientConstraint(master, slave, n= inputName)
    else:
        orC= mc.orientConstraint(master, slave, n= inputName, w= weight)
    mc.hide(orC)
    return orC

def create_aim_constraint(master, slave, inputName, weight= 1, upObject= 'none', reversed= False):
    """ inputs: master, slave, name, upObject; outputs: """
    
    if reversed:
        aimDirection= (1,0,0)
    else:
        aimDirection= (-1,0,0)
    
    
    if upObject is 'none':
        if weight is 1:
            aiC= mc.aimConstraint( master, slave, n= inputName, aim= aimDirection)
        else:
            aiC= mc.aimConstraint( master, slave, n= inputName, w= weight, aim= aimDirection)
    else:
        if weight is 1:
            aiC= mc.aimConstraint( master, slave, n= inputName, worldUpType= 'object', worldUpObject= upObject, aim= aimDirection)
        else:
            aiC= mc.aimConstraint( master, slave, n= inputName, worldUpType= 'object', worldUpObject= upObject, w= weight, aim= aimDirection)
    mc.hide(aiC)
    return aiC



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ add Messages
def cp_createNodeWithoutHistory(nodeType, n= 'nodeName', ss= False):
    import maya.cmds as mc
    name= n
    skipSelection= ss
    createdNode= mc.createNode(nodeType, n= name, ss= skipSelection)
    mc.setAttr('{0}.isHistoricallyInteresting'.format(createdNode), 0)
    return createdNode

def addAndConnectJointsToMessage(inputNode, inputAttr):
    attrName= 'isJointOf'
    isJntOfAttr= mc.addAttr(inputNode, longName= attrName, attributeType= 'message')
    mc.connectAttr(inputAttr, '{0}.{1}'.format(inputNode, attrName))
    
    
    
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ add Messages

def create_message_connections(master, input):
    """ documentation """

    if isinstance(input, list):
        for i in input:
            mc.addAttr(i, longName= 'connectionInformation', attributeType= 'message')
            mc.connectAttr(master[1], i+'.connectionInformation')
    else:
        mc.addAttr(input, longName= 'connectionInformation', attributeType= 'message')
        mc.connectAttr(master[1], input+ '.connectionInformation')
        
        
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ set joint lables
#IMPORT THAT SCRIPT
        

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ set override
def set_override(list):
    for i in list:
        try:
            shapeNodes= mc.listRelatives (s= True)
            for shape in shapeNodes:
                mc.setAttr('{name}.overrideEnabled'.format(name= shape), 1)
                mc.setAttr('{name}.overrideDisplayType'.format(name= shape), 1) 
        except:
            mc.setAttr('{name}.overrideEnabled'.format(name= i), 1)
            mc.setAttr('{name}.overrideDisplayType'.format(name= i), 1) 


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ format shape nodes
def format_shape_node(input, cp_allowedPrefixes):
    curveName = input
    
    if '_' in input:
        
        #### if it does not work on fringe cases try with index + copy etc
        splitString= input.split('_')
        
        middleString = splitString[1]
        
        lengthCheck= len(splitString)
        
        if lengthCheck > 3:
            middleString='{0}_{1}'.format(middleString, splitString[2])
        
        formatedPrefix= '{0}_'.format(splitString[0])
        formatedPrefix= formatedPrefix.capitalize()
        
        
        if formatedPrefix in cp_allowedPrefixes:
            
            #print 'prefix of it is "{0}"'.format(formatedPrefix)
            
            if formatedPrefix[0] == 'L':
                curveColor= dictColorValues['left']
            
            if formatedPrefix[0] == 'R':
                curveColor= dictColorValues['right']

            if formatedPrefix[0] == 'C':
                curveColor= dictColorValues['center']

        else:
            curveColor= 13

    shapeNodes= mc.listRelatives(curveName, s= True)
    for i in shapeNodes:
        newShapeName= mc.rename(i, curveName+ 'Shape')
        mc.setAttr('{name}.overrideEnabled'.format(name= newShapeName), 1)
        mc.setAttr('{name}.overrideColor'.format(name= newShapeName), curveColor) 
    
    return newShapeName

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ connect Visibility
    
    nodeName= inputName
    
    outputNode= cp_createNodeWithoutHistory('reverse', n= secondName)
    
    if nodeExists != 'node':
        outputNode= cp_createNodeWithoutHistory('multDoubleLinear', n= nodeName)
        mc.setAttr('{name}.input2'.format(name= nodeName), 0.1)
        #mc.connectAttr('{name}.'.format(), '')
        mc.connectAttr('{name}.input1'.format(name= inputName), '{name}.input1'.format(name= outputNode))
    #mc.connectAttr()

#connect_visibility('baba', 'secondName', 'gah')

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ reset Controls
def reset_controls(inputNode):
    """ documentation """
    for i in inputNode:
        damn= mc.listConnections(i, source= True, destination= False)
        matching = [i for i in damn if 'master' in i]
        if len(matching) == 1:
            print 'yes'


            for i in (mc.listConnections('{0}.connectedControls'.format(matching[0]))):
                axesList= ['x','y','z']
                for axes in axesList:
                    
                    try:
                        mc.setAttr('{node}.t{ending}'.format(node= i, ending= axes), 0)
                    except: 
                        pass
                    
                    try:
                        mc.setAttr('{node}.r{ending}'.format(node= i,ending= axes), 0)
                    except: 
                        pass
                    
                    try:
                        mc.setAttr('{node}.s{ending}'.format(node= i,ending= axes), 0)
                    except: 
                        pass
                
                customAttrs= mc.listAttr(userDefined= True, write= True, settable= True)
                for attr in customAttrs:
                    try:
                        mc.setAttr('{node}.{ending}'.format(node= i, ending= attr), 0)
                    except:
                        pass
                
    mc.select(clear=True)
#reset_controls(mc.ls(sl= True))

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ vec calculation

def triangle_vector_ops(inputList):
    """ output one: pointPosition; output two: vectors; output three: magnitudes; """
    
    import maya.cmds as mc
    import maya.OpenMaya as om
    

    list= []
    
    for i in inputList:
        position= tuple(mc.xform(i, t= True, worldSpace= True, q= True))
        list.append(position)
    
    firstItem= list[:1]
    firstItemTuple= firstItem[0]
    
    middleItem= float(len(list))/2
    if middleItem %2 != 0:
        middleItem= list[int(middleItem - 0.5)]
    else:
        middleItem(list[int(middleItem)], list[int(middleItem-1)])
        
    lastItem= list[-1:]
    lastItemTuple= lastItem[0]
    
    
    
    start_vec= om.MVector(firstItemTuple[0], firstItemTuple[1], firstItemTuple[2])
    middle_vec= om.MVector(middleItem[0], middleItem[1], middleItem[2])
    end_vec= om.MVector(lastItemTuple[0], lastItemTuple[1], lastItemTuple[2])
    
    
    
    startTOend_vec= (end_vec - start_vec)
    startTOmiddle_vec= (middle_vec - start_vec)
    middleTOend_vec=(end_vec - middle_vec)
    
    
    
    startTOend_mag= startTOend_vec.length()
    startTOmiddle_mag= startTOmiddle_vec.length()
    middleTOend_mag= middleTOend_vec.length()
    
    
    positionOutput= list
    pointvectorOutput= [(start_vec[0],start_vec[1],start_vec[2]), (middle_vec[0], middle_vec[1], middle_vec[2]), (end_vec[0], end_vec[1], end_vec[2])]
    
    vectorOutput= [(startTOmiddle_vec[0], startTOmiddle_vec[1], startTOmiddle_vec[2]), (middleTOend_vec[0], middleTOend_vec[1], middleTOend_vec[2]), (startTOend_vec[0], startTOend_vec[1], startTOend_vec[2])]
    magnitudesOutput= [startTOmiddle_mag, middleTOend_mag, startTOend_mag]
    
    return positionOutput, vectorOutput, magnitudesOutput
    
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ jnt duplication
def duplicate_joints(inputValue, newNameList, jointSize):
    """ documentation """
    
    
    if isinstance(inputValue, list) == False:
        inputList= []
        inputList.append(inputValue)
        
        newNames= []
        newNames.append(newNameList)
    
    else:
        inputList= inputValue
        newNames= newNameList
    
    listOne= mc.duplicate(inputList, n= 'toRename', po= True)
    
    duplicatedJoints = []
    
    for position, i in enumerate (listOne):
        result = mc.rename(i, newNames[position])
        duplicatedJoints.append(result)
        mc.setAttr('{name}.radius'. format(name= result) , jointSize)
    mc.select(clear= True)    
    
    return duplicatedJoints

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ connectVisibility
def connect_visibility(inputList, nodeName1, nodeName2):

    import maya.cmds as mc 
    #~~~~~~~~~~~~~~~~~~~~~~ gather info
    connectedControls= mc.listConnections('{0}.connectedControls'.format(inputList))
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~ names
    matching = [s for s in connectedControls if "BlenderArm_ctrl" in s]
    
    #~~~~~~~~~~~~~~~~~~~~~~~~ operation
    
    
    if mc.objExists(nodeName2) == False:
        reverseNode=cp_createNodeWithoutHistory('reverse', n= nodeName2)
    
    if mc.objExists(nodeName1) == False:

        decimNode=cp_createNodeWithoutHistory('multDoubleLinear', n= nodeName1)
        
        mc.setAttr('{name}.input2'.format(name= decimNode), 0.1)
        mc.connectAttr('{name}.ikFkBlend'.format(name= matching[0]), '{name}.input1'.format(name= nodeName1))
        mc.connectAttr('{name}.output'.format(name= decimNode), '{name}.inputX'.format(name= nodeName2))
        
        for i in connectedControls:
            if 'ik' in i:
                mc.connectAttr('{name}.output'.format(name= decimNode), '{name}.visibility'.format(name= i))
            
            elif 'fk' in i:
                mc.connectAttr('{name}.outputX'.format(name= reverseNode), '{name}.visibility'.format(name= i))
            
            else:
                pass
    else:
        mc.connectAttr('{name}.output'.format(name= nodeName1), '{name}.inputX'.format(name= nodeName2))
        for i in connectedControls:
            if 'ik' in i:
                mc.connectAttr('{name}.output'.format(name= nodeName1), '{name}.visibility'.format(name= i))
            
            elif 'fk' in i:                
                mc.connectAttr('{name}.outputX'.format(name= nodeName2), '{name}.visibility'.format(name= i))

            else:
                pass
    
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ fk
def create_fk_rig(inputNameList, inputObjectList, limb= 'arm'):
    """ documentation """
    #~~~~~~~~~~~~~~~~~~~~~~~~ input needs cleanup  
    cp_limb= limb  

    ## CREATE FK NAMES FOR CONTROLS AND OFFSET ##
    orConstraintName= format_object_name(change_strings_by_list(1, inputObjectList[0], cp_limb)[0], 'orC')
    
    def create_fk_names(limb= 'arm'):
        """ input: string= 'arm', references dict """
        
        cp_limb= limb
        dictBodyParts[cp_limb][0]
        namesList= [dictBodyParts[cp_limb][0], dictBodyParts[cp_limb][2], dictBodyParts[cp_limb][4]]
        
        
        fkCtrlNames= []
        constraintNames= []
        pointConstraintNames=[]

        for position, item in enumerate(namesList):
        
            renamedInput= format_object_name(change_strings_by_list(1, inputObjectList[0], item)[0], 'ctrl', 'fk')
            fkCtrlNames.append(renamedInput)
            
            if position < (len(namesList)-1):
                aimConstraintName= format_object_name(change_strings_by_list(1, inputObjectList[0], item)[0], 'aiC', 'fk')
                constraintNames.append(aimConstraintName)
            else:
                orientConstraintName= format_object_name(change_strings_by_list(1, inputObjectList[0], item)[0], 'orC', 'fk')
                constraintNames.append(orientConstraintName)

            pointConstraintName = format_object_name(change_strings_by_list(1, inputObjectList[0], item)[0], 'ptC', 'fk')
            pointConstraintNames.append(pointConstraintName)



        fkOffsetNames= []
        for i in (fkCtrlNames):
            renamedInput = format_object_name(i,'srtOffset')
            fkOffsetNames.append(renamedInput)
        
        upAimNames= []
        for i in (fkCtrlNames):
            renamedInput = format_object_name(i,'pos')
            upAimNames.append(renamedInput)
        

            
        return fkCtrlNames, fkOffsetNames, constraintNames, upAimNames, pointConstraintNames
    
    #~~~~~~~~~~~~~~~~~~~~~~~~ input needs cleanup  
    fkJoints= duplicate_joints(inputObjectList, inputNameList, .1)
    
    
    

    fkNaming= create_fk_names(cp_limb)


    jointList= fkJoints

    controlsList= []
    offsetList= []
    upVecNodesList=[]


    
    for position, i in enumerate(fkJoints):
        
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~ create curve
        curveName = fkNaming[0][position]
        offsetName = fkNaming[1][position]
        upVecName = fkNaming[3][position]

        curveNode= mc.curve(n= curveName, p= fkCurve )
        offsetNode = cp_createNodeWithoutHistory('transform', n= offsetName)
        upVecNode = cp_createNodeWithoutHistory('transform', n= upVecName)


        
        mc.xform(curveNode, r=True, ro=(0, 0, -90))
        mc.xform(curveNode, r=True, s=(.5, .5, .5))
        mc.makeIdentity( curveNode, apply=True, rotate=True, scale= True )
        
        mc.addAttr(curveNode, shortName='stretch', longName='jointStretch', defaultValue=10, hsn= True, hsx = True, smn= 0, smx= 10, keyable= True)
        
        axesList= ['x','y','z']
        for axes in axesList:        
            mc.setAttr('{node}.s{ending}'.format(node= curveNode, ending= axes), lock= True, keyable= False, channelBox= False)
        
        format_shape_node(curveName, cp_allowedPrefixes)
        

        
        
        mc.parent(curveNode, offsetNode, absolute= True)
        
        #~~~~~~~~~~~ match curve pos with jnt pos
        mc.matchTransform(offsetNode, fkJoints[position])
        
        mc.matchTransform(upVecNode, fkJoints[position])
        mc.xform(upVecNode, r=True, os= True, t=(0, 1, 0))
        mc.makeIdentity(upVecNode, apply=True, rotate=True, scale= True )

        #~~~~~~~~~~~~~ connect controls to joints

        #~~~~~~~~~~~~~~~~~~~~~~ prepare parenting
        controlsList.append(curveNode)
        offsetList.append(offsetNode)
        upVecNodesList.append(upVecNode)
        
        
        
    for position, i in enumerate(jointList):
        rotationConstraintName = fkNaming[2][position]
        positionConstraintName= fkNaming[4][position]
        
        create_point_constraint(controlsList[position], fkJoints[position], positionConstraintName, weight= 1)
        
        if position < len(jointList)-1:
            newListPos= position+1
            if newListPos < len(jointList):
                
                checkXaxis= mc.xform(jointList[1],t= True, r= True, q= True)[0]
                
                if checkXaxis > 0:
                    checkXaxisBool = True
                elif checkXaxis < 0:
                    checkXaxisBool = False
                else:
                    checkXaxisBool = None
            
            
                if checkXaxisBool:
                    reversedDirection= True
                else:
                    reversedDirection= False
                create_aim_constraint(controlsList[newListPos], fkJoints[position], rotationConstraintName, weight= 1, upObject= upVecNodesList[position], reversed=reversedDirection)
        else:
            create_orient_constraint(controlsList[position], fkJoints[position], rotationConstraintName)

    for pos, i in enumerate(jointList):
        mc.parent(upVecNodesList[pos], controlsList[pos])
    
    
    
    mc.parent(offsetList[1], controlsList[0])
    mc.parent(offsetList[2], controlsList[1])
    
    
    return fkJoints, controlsList
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ik
def create_curve(inputList, curveName, curveShape, freezeT= False, freezeO= False, freezeS= False):
    """ inputs: list= , curveName, curveShape """
    
    objectTranslate= mc.xform(inputList, q= True, r= True, t= True, ws= True)
    objectRotate= mc.xform(inputList, q= True, r= True, ro= True, ws= True)    
    curveNode= mc.curve(n= curveName, d= 1, p= curveShape)
    
    newShapeName= format_shape_node(curveNode, cp_allowedPrefixes)
    
    axesList= ['x','y','z']
    for axes in axesList:
        
        if freezeT == True:
            mc.setAttr('{node}.t{ending}'.format(node= curveNode, ending= axes), lock= True, keyable= False, channelBox= False)
        
        if freezeO == True:
            mc.setAttr('{node}.r{ending}'.format(node= curveNode, ending= axes), lock= True, keyable= False, channelBox= False)    
        
        if freezeS == True:
            mc.setAttr('{node}.s{ending}'.format(node= curveNode, ending= axes), lock= True, keyable= False, channelBox= False)    
    
    offset= mc.group( curveNode, name= format_object_name(curveName, 'srtOffset'))
    
    mc.xform(offset, r= True, t= objectTranslate)
    mc.xform(offset, r= True, ro= objectRotate)
    

    return curveNode


def create_curve_with_object_connection(transforms, curveName, objectName, curveShape, freezeT= False, freezeO= False, freezeS= False):
    """ input: list= transforms[tuple= translate, tuple= orient], string= curveName, string= objectName, list= curveShape; output: """
    
    pvCurve= mc.curve(n= curveName, d= 1, p= curveShape)
    offsetGrp= mc.group( pvCurve, name= format_object_name(curveName, 'srtOffset'))
    
    axesList= ['x','y','z']
    for axes in axesList:
        
        if freezeT == True:
            mc.setAttr('{node}.t{ending}'.format(node= pvCurve, ending= axes), lock= True, keyable= False, channelBox= False)
        
        if freezeO == True:
            mc.setAttr('{node}.r{ending}'.format(node= pvCurve, ending= axes), lock= True, keyable= False, channelBox= False)    
        
        if freezeS == True:
            mc.setAttr('{node}.s{ending}'.format(node= pvCurve, ending= axes), lock= True, keyable= False, channelBox= False) 
    
    newShapeName= format_shape_node(pvCurve, cp_allowedPrefixes)
    
    shapeNodes= mc.listRelatives (pvCurve, s= True)    
    for i in shapeNodes:
        decMtx= '{name}_dMtx'.format(name= newShapeName)
        multMtx= '{name}_mMtx'.format(name= newShapeName)
        amount= mc.getAttr('{name}.cp'.format(name= newShapeName), s=1)
        endPosition= amount-1
        
        
        cp_createNodeWithoutHistory('multMatrix', n=multMtx)
        cp_createNodeWithoutHistory('decomposeMatrix', n=decMtx)
        
        mc.connectAttr('{nameOne}.worldMatrix[0]'.format(nameOne= objectName), '{nameTwo}.matrixIn[0]'.format(nameTwo= multMtx))
        mc.connectAttr('{nameOne}.worldInverseMatrix[0]'.format(nameOne= pvCurve), '{nameTwo}.matrixIn[1]'.format(nameTwo= multMtx))
        mc.connectAttr('{nameOne}.matrixSum'.format(nameOne= multMtx), '{nameTwo}.inputMatrix'.format(nameTwo= decMtx))
        mc.connectAttr('{name}.outputTranslate'.format(name= decMtx), '{result}.controlPoints[{endPoints}]'.format(result= newShapeName, endPoints= str(endPosition)))
    
    
    
    mc.xform(offsetGrp, a= True, t= transforms[0])
    mc.xform(offsetGrp, a= True, ro= transforms[1])
    
    return pvCurve, offsetGrp

def create_ikH(inputList, inputString):
    """ input: list= jointList, ; output: string= ikHandle; function: loads ikSolver and creates ikHandle """
    
    firstItem= inputList[:1][0]
    lastItem= inputList[-1:][0]
    
    mel.eval('ik2Bsolver;')
    handle = mc.ikHandle(sj= firstItem, ee=lastItem, sol='ik2Bsolver', n= inputString)
    mc.hide(handle)
    
    return handle
    
def calc_pv_pos(inputList, scalingFactor):
    """ input: list= 3 objects, float= scalingFactor; output: tuple= poleV_position, tuple= poleV_rotation; function: calculates the polevector between three objects, returns point based on scalingFactor """

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ calculate Output
    basicCalcs= triangle_vector_ops(inputList)
    start_vec = om.MVector(basicCalcs[0][0][0], basicCalcs[0][0][1], basicCalcs[0][0][2])
    middle_vec = om.MVector(basicCalcs[0][1][0], basicCalcs[0][1][1], basicCalcs[0][1][2])
    end_vec = om.MVector(basicCalcs[0][2][0], basicCalcs[0][2][1], basicCalcs[0][2][2])
    
    startTOend_vec = (end_vec - start_vec)
    startTOmiddle_vec = (middle_vec - start_vec)
    middleTOend_vec = (end_vec - middle_vec)
    
    scaleValue = (startTOend_vec * startTOmiddle_vec) / (startTOend_vec * startTOend_vec)
    
    proj_point = (startTOend_vec * scaleValue)+ start_vec
    
    startTOend_len = basicCalcs[2][2]
    
    custom_len = startTOend_len * scalingFactor
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~ calculate translate
    poleV_position_mvec = ((middle_vec - proj_point).normal() *   custom_len) + middle_vec
    
    poleV_position= poleV_position_mvec[0], poleV_position_mvec[1], poleV_position_mvec[2]
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ calculate rotation
    temp_geo= mc.polyCube()
    mc.move(poleV_position[0], poleV_position[1], poleV_position[2])
    temp_const= mc.aimConstraint(inputList[1], temp_geo, aimVector= (-1,0,0), worldUpType= 'object', wuo= inputList[0], upVector=(0,0,-1))
    poleV_rotation= mc.xform(temp_geo, ro= True, q= True)
    mc.delete(temp_geo, temp_const)
    
    
    
    return poleV_position, poleV_rotation

def create_middle_pos(inputList, inputControl01, inputControl02, inputAimUp= 'none'):
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~ get constraint weights
    len01= triangle_vector_ops(inputList)[2][0]
    len02= triangle_vector_ops(inputList)[2][1]
    gesLen= len01 + len02
    firstWeight= len02/gesLen
    secondWeight= len01/gesLen
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ set up groups
    pvPoint_tOffset= cp_createNodeWithoutHistory('transform', n= 'Lf_pvPoint_tOffset')
    pvAim_rtOffset= mc.group( pvPoint_tOffset, n='Lf_pvAim_rtOffset' )
    pvBase_rtOffset= mc.group( pvAim_rtOffset, n='Lf_pvBase_rtOffset' )
    
    mc.matchTransform(pvBase_rtOffset,inputControl01)
    
    create_aim_constraint(inputControl02, pvAim_rtOffset, 'Lf_pvArm_aiC', upObject= inputAimUp)
    create_point_constraint(inputControl01, pvPoint_tOffset, 'Lf_pvArm_ptC', weight= secondWeight)
    create_point_constraint(inputControl02, pvPoint_tOffset, 'Lf_pvArm_ptC', weight= firstWeight)
    
    
    print len01/gesLen, len02/gesLen
    return 'yxo'

#create_middle_pos(mc.ls(sl= True), 'Lf_ikArm_ctrl', 'Lf_atopShoulder_RIG', 'locator1')

def create_ik_rig(inputNameList, inputObjectList, limb= 'arm'):
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ declare variables
    cp_limb= limb
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ generate ik names
    pvCurveName = format_object_name(change_strings_by_list(1, inputObjectList[0], cp_limb)[0], 'ctrl', 'ikPv')
    pvConstraintName = format_object_name(change_strings_by_list(1, inputObjectList[0], cp_limb)[0], 'pvC')
    ptConstraintName = format_object_name(change_strings_by_list(1, inputObjectList[0], cp_limb)[0], 'ptC')
    
    
    ikCurveName = format_object_name(change_strings_by_list(1, inputObjectList[0], cp_limb)[0], 'ctrl', 'ik')
    ikHandleName = format_object_name(change_strings_by_list(1, inputObjectList[0], cp_limb)[0], 'ikH')
    orConstraintNameIk= format_object_name(change_strings_by_list(1, inputObjectList[0], cp_limb)[0], 'orC', 'ik')
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ generate ik names
    controlsList= []
    
    ikJoints= duplicate_joints(inputObjectList, inputNameList, .1)
    
    
    ikControl= create_curve(ikJoints[-1:][0], ikCurveName, ikCurve, freezeS= True)
    ikHandleObj= create_ikH(ikJoints, ikHandleName)
    
    pvTransforms = calc_pv_pos(inputObjectList,0.5)

    pvCurve = create_curve_with_object_connection(pvTransforms, pvCurveName, inputNameList[1], diamondCurve, freezeO= True, freezeS= True)[0]
    
    create_point_constraint(ikControl, ikHandleName, ptConstraintName)
    create_pv_constraint(pvCurve, ikHandleName, pvConstraintName)
    create_orient_constraint(ikControl, ikJoints[-1:][0], orConstraintNameIk)
    
    controlsList.append(ikControl)
    controlsList.append(pvCurve)
    
    return ikJoints, controlsList

#create_ik_stretch(ikControlsCurves[0], 'Lf_ikShoulder_RIG', blendMaster[0], masterNode)

def create_ik_stretch(inputList, inputObj0, inputObj2, inputLocalMaster, limbInput):
    """ input """
    
    
    firstJointTranslate= mc.getAttr('{0}.translateX'.format(inputList[1]))
    secondJointTranslate= mc.getAttr('{0}.translateX'.format(inputList[2]))
    translatesAdd= firstJointTranslate + secondJointTranslate
    
    print translatesAdd
    #~~~~~~~~~~~~~~~~~~~~~~~~~ get external names
    masterNode= inputLocalMaster[0]
    masterConnections= inputLocalMaster[1]
    masterConnections= mc.listConnections(masterConnections)
    
    cp_limb= limbInput.capitalize()
    limbName= 'ik{limb}'.format(limb= cp_limb)
    
    upperLimbName= 'up{limb}'.format(limb= cp_limb)
    lowerLimbName= 'low{limb}'.format(limb= cp_limb)
    
    blenderControlName= [s for s in masterConnections if "Blender{limb}_ctrl".format(limb= cp_limb) in s]
    blenderControlName= blenderControlName[0]
    
    ikHandleControlName= [s for s in masterConnections if "ik{limb}_ctrl".format(limb= cp_limb) in s]
    ikHandleControlName= ikHandleControlName[0]
    ikHandleName= (ikHandleControlName.split('_')[1])+'Ctrl'
    
    pvHandleControlName= [s for s in masterConnections if "ikPv{limb}_ctrl".format(limb= cp_limb) in s]
    pvHandleControlName= pvHandleControlName[0]
    pvHandleName= pvHandleControlName.split('_')[1]+'Ctrl'
    
    
    splicedNames= []
    for i in inputList:
        splicedName= i.split('_')
        splicedNames.append(splicedName)
    
    prefix= splicedNames[0][0]
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ generate names

    distanceNodeNamesStretch=[]
    for i in range(0,2):
        distanceNodeName= '{prefix}_{firstName}_TO_{secondName}_dist'.format(prefix= prefix, firstName= splicedNames[i][1], secondName= splicedNames[i+1][1])
        distanceNodeNamesStretch.append(distanceNodeName)
    
    distanceNodeNamesLock01= '{prefix}_{firstName}_TO_{secondName}_dist'.format(prefix= prefix, firstName= 'ikShoulder', secondName= pvHandleName)
    
    distanceNodeNamesLock02= '{prefix}_{firstName}_TO_{secondName}_dist'.format(prefix= prefix, firstName= pvHandleName, secondName= ikHandleName)

    controlDistNode= '{prefix}_{firstName}_TO_{secondName}_dist'.format(prefix= prefix, firstName= 'ikShoulder', secondName= ikHandleName)
    
    
    divLockName= '{prefix}_{firstName}_TO_{secondName}_div'.format(prefix= prefix, firstName= 'lock', secondName= splicedNames[i+1][1])
        
    addName= '{prefix}_{limb}_{name}_add'.format(prefix= prefix, limb= limbName, name= 'decim')
    
    divName= '{prefix}_{firstName}_TO_{secondName}_div'.format(prefix= prefix, firstName= splicedNames[i][1], secondName= splicedNames[i+1][1])
    
    blendTwoAttrName= '{prefix}_{firstName}_TO_{secondName}_bta'.format(prefix= prefix, firstName= 'ikShoulder', secondName= ikHandleName)
    
    conditionName= '{prefix}_{firstName}_TO_{secondName}_cnd'.format(prefix= prefix, firstName= splicedNames[i][1], secondName= splicedNames[i+1][1])
    
    decimationName= '{prefix}_{limb}_{name}_mult'.format(prefix= prefix, limb= limbName, name= 'decim')
    
    multUpArmName=  '{prefix}_{limb}_{name}_mult'.format(prefix= prefix, limb= limbName, name= upperLimbName)
    
    multLowArmName=  '{prefix}_{limb}_{name}_mult'.format(prefix= prefix, limb= limbName, name= lowerLimbName)
    
    rigScaleName= '{prefix}_{limb}_{name}_mult'.format(prefix= prefix, limb= limbName, name= 'rigScale')
    
    reverseName= '{prefix}_{limb}_{name}_rev'.format(prefix= prefix, limb= limbName, name= 'reverseScale')
    
    decimationLockName= '{prefix}_{limb}_{name}_mult'.format(prefix= prefix, limb= limbName, name= 'decimLock')
    
    revLockName= '{prefix}_{limb}_{name}_rev'.format(prefix= prefix, limb= limbName, name= 'reverseScaleLock')
    
    blendTwoAttrOneName= '{prefix}_{limb}_{name}_bta'.format(prefix= prefix, limb= limbName, name= 'upArmLock')
    
    blendTwoAttrTwoName= '{prefix}_{limb}_{name}_bta'.format(prefix= prefix, limb= limbName, name= 'lowArmLock')
    
    rigScaleLockName= '{prefix}_{limb}_{name}_div'.format(prefix= prefix, limb= limbName, name= 'rigScaleLock')
    
    multTranslateRevName01= '{prefix}_{limb}_{name}_mult'.format(prefix= prefix, limb= limbName, name= 'jointTransRev01')
    multTranslateRevName02= '{prefix}_{limb}_{name}_mult'.format(prefix= prefix, limb= limbName, name= 'jointTransRev02')
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ build stretch
    addNode= cp_createNodeWithoutHistory('addDoubleLinear', n= addName)
    
    scaleNode= cp_createNodeWithoutHistory('multDoubleLinear', n= rigScaleName)
    
    distanceNodeOne= cp_createNodeWithoutHistory('distanceBetween', n= distanceNodeNamesStretch[0])
    mc.connectAttr('{0}.shoulderMatrix'.format(masterNode), '{0}.inMatrix1'.format(distanceNodeOne))
    mc.connectAttr('{0}.elbowMatrix'.format(masterNode), '{0}.inMatrix2'.format(distanceNodeOne))
    mc.connectAttr('{0}.distance'.format(distanceNodeOne), '{0}.input1'.format(addName))
    
    distanceNodeTwo= cp_createNodeWithoutHistory('distanceBetween', n= distanceNodeNamesStretch[1])
    mc.connectAttr('{0}.elbowMatrix'.format(masterNode), '{0}.inMatrix1'.format(distanceNodeTwo))
    mc.connectAttr('{0}.wristMatrix'.format(masterNode), '{0}.inMatrix2'.format(distanceNodeTwo))
    mc.connectAttr('{0}.distance'.format(distanceNodeTwo), '{0}.input2'.format(addName))
    
    
    controlDistNode= cp_createNodeWithoutHistory('distanceBetween', n= controlDistNode)
    mc.connectAttr('{0}.worldMatrix[0]'.format(inputObj0), '{0}.inMatrix1'.format(controlDistNode))
    mc.connectAttr('{0}.worldMatrix[0]'.format(ikHandleControlName), '{0}.inMatrix2'.format(controlDistNode))
    
    divNode= cp_createNodeWithoutHistory('multiplyDivide', n= divName)
    mc.setAttr('{0}.operation'.format(divNode), 2, lock= True)
    mc.connectAttr('{0}.distance'.format(controlDistNode), '{0}.input1X'.format(divNode))
    mc.connectAttr('{0}.output'.format(scaleNode), '{0}.input2X'.format(divNode))

    mc.addAttr(blenderControlName, shortName='stretch', longName='stretchFactor', defaultValue=0, hasSoftMaxValue= True, hasSoftMinValue= True, softMinValue= 0, softMaxValue=10, keyable= True)
    
    decimationNode= create_decimisation_nodes(decimationName)
    mc.connectAttr('{0}.stretchFactor'.format(blenderControlName), '{0}'.format(decimationNode[1]))
    
    reverseNode= cp_createNodeWithoutHistory('reverse', n= reverseName)
    mc.connectAttr('{0}'.format(decimationNode[2]), '{0}.inputX'.format(reverseNode))
    
    blendTwoAttrNode= cp_createNodeWithoutHistory('blendTwoAttr', n= blendTwoAttrName)
    mc.connectAttr('{0}.outputX'.format(divNode), '{0}.input[0]'.format(blendTwoAttrNode))
    mc.connectAttr('{0}.outputX'.format(reverseNode), '{0}.attributesBlender'.format(blendTwoAttrNode))
    mc.setAttr('{0}.input[1]'.format(blendTwoAttrNode), 1, lock= True)
    
    conditionNode= cp_createNodeWithoutHistory('condition', n= conditionName)
    mc.connectAttr('{0}.output'.format(blendTwoAttrNode), '{0}.colorIfTrueR'.format(conditionNode))
    
    mc.connectAttr('{0}.output'.format(addNode), '{0}.input1'.format(scaleNode))
    mc.connectAttr('{0}.rigScale'.format(masterNode), '{0}.input2'.format(scaleNode))
    mc.connectAttr('{0}.output'.format(scaleNode), '{0}.firstTerm'.format(conditionNode))
    mc.connectAttr('{0}.distance'.format(controlDistNode), '{0}.secondTerm'.format(conditionNode))
    
    mc.setAttr('{0}.colorIfFalseR'.format(conditionNode), 1, lock= True)
    mc.setAttr('{0}.operation'.format(conditionNode), 5, lock= True)
    
    multNodeUpArm= cp_createNodeWithoutHistory('multDoubleLinear', n= multUpArmName)
    mc.connectAttr('{0}.upperArmLength'.format(masterNode), '{0}.input2'.format(multNodeUpArm))
    
    multNodeLowArm= cp_createNodeWithoutHistory('multDoubleLinear', n= multLowArmName)
    mc.connectAttr('{0}.lowerArmLength'.format(masterNode), '{0}.input2'.format(multNodeLowArm))
    
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ build lock
    divLockNode= cp_createNodeWithoutHistory('multiplyDivide', n= divLockName)
    mc.setAttr('{0}.operation'.format(divLockNode), 2, lock= True)

    rigScaleLockNode= cp_createNodeWithoutHistory('multiplyDivide', n= rigScaleLockName)
    mc.setAttr('{0}.operation'.format(rigScaleLockNode), 2)    
    
    distanceLockNodeOne= cp_createNodeWithoutHistory('distanceBetween', n= distanceNodeNamesLock01)
    mc.connectAttr('{0}.worldMatrix[0]'.format(inputObj0), '{0}.inMatrix1'.format(distanceLockNodeOne))
    mc.connectAttr('{0}.worldMatrix[0]'.format(pvHandleControlName), '{0}.inMatrix2'.format(distanceLockNodeOne))
    
    mc.connectAttr('{0}.distance'.format(distanceLockNodeOne), '{0}.input1X'.format(rigScaleLockNode))
    mc.connectAttr('{0}.rigScale'.format(masterNode), '{0}.input2X'.format(rigScaleLockNode))
    mc.connectAttr('{0}.outputX'.format(rigScaleLockNode), '{0}.input1X'.format(divLockNode))
    mc.connectAttr('{0}.distance'.format(distanceNodeOne), '{0}.input2X'.format(divLockNode))

    distanceLockNodeTwo= cp_createNodeWithoutHistory('distanceBetween', n= distanceNodeNamesLock02)
    mc.connectAttr('{0}.worldMatrix[0]'.format(pvHandleControlName), '{0}.inMatrix1'.format(distanceLockNodeTwo))
    mc.connectAttr('{0}.worldMatrix[0]'.format(ikHandleControlName), '{0}.inMatrix2'.format(distanceLockNodeTwo))

    mc.connectAttr('{0}.distance'.format(distanceLockNodeTwo), '{0}.input1Y'.format(rigScaleLockNode))
    mc.connectAttr('{0}.rigScale'.format(masterNode), '{0}.input2Y'.format(rigScaleLockNode))
    mc.connectAttr('{0}.outputY'.format(rigScaleLockNode), '{0}.input1Y'.format(divLockNode))
    mc.connectAttr('{0}.distance'.format(distanceNodeTwo), '{0}.input2Y'.format(divLockNode))

    mc.addAttr(blenderControlName, shortName='lock', longName='pvLock', defaultValue=0, hasSoftMaxValue= True, hasSoftMinValue= True, softMinValue= 0, softMaxValue=10, keyable= True)
    
    decimationLockNode= create_decimisation_nodes(decimationLockName)
    mc.connectAttr('{0}.pvLock'.format(blenderControlName), '{0}'.format(decimationLockNode[1]))

    revLockNode= cp_createNodeWithoutHistory('reverse', n= revLockName)
    mc.connectAttr('{0}'.format(decimationLockNode[2]), '{0}.inputX'.format(revLockNode))
    
    blendTwoAttrOneNode= cp_createNodeWithoutHistory('blendTwoAttr', n= blendTwoAttrOneName)
    mc.connectAttr('{0}.outputX'.format(divLockNode), '{0}.input[0]'.format(blendTwoAttrOneNode))
    mc.connectAttr('{0}.outColorR'.format(conditionNode), '{0}.input[1]'.format(blendTwoAttrOneNode))
    mc.connectAttr('{0}.output'.format(blendTwoAttrOneNode), '{0}.input1'.format(multNodeUpArm))

    mc.connectAttr('{0}.outputX'.format(revLockNode), '{0}.attributesBlender'.format(blendTwoAttrOneNode))
    
    blendTwoAttrTwoNode= cp_createNodeWithoutHistory('blendTwoAttr', n= blendTwoAttrTwoName)
    mc.connectAttr('{0}.outputY'.format(divLockNode), '{0}.input[0]'.format(blendTwoAttrTwoNode))
    mc.connectAttr('{0}.outColorR'.format(conditionNode), '{0}.input[1]'.format(blendTwoAttrTwoNode))
    mc.connectAttr('{0}.output'.format(blendTwoAttrTwoNode), '{0}.input1'.format(multNodeLowArm))
    
    mc.connectAttr('{0}.outputX'.format(revLockNode), '{0}.attributesBlender'.format(blendTwoAttrTwoNode))
    
    

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ build Network
    
    if translatesAdd >= 0:
        mc.connectAttr('{0}.output'.format(multNodeUpArm), '{0}.translateX'.format(inputList[1]))
        mc.connectAttr('{0}.output'.format(multNodeLowArm), '{0}.translateX'.format(inputList[2]))
    else:
        multTranslateRevNode01= cp_createNodeWithoutHistory('multDoubleLinear', n= multTranslateRevName01)
        mc.setAttr('{0}.input2'.format(multTranslateRevNode01), -1, lock= True)
        
        mc.connectAttr('{0}.output'.format(multNodeUpArm), '{0}.input1'.format(multTranslateRevNode01))
        mc.connectAttr('{0}.output'.format(multTranslateRevNode01), '{0}.translateX'.format(inputList[1]))
        
        
        multTranslateRevNode02= cp_createNodeWithoutHistory('multDoubleLinear', n= multTranslateRevName02)
        mc.setAttr('{0}.input2'.format(multTranslateRevNode02), -1, lock= True)
        
        mc.connectAttr('{0}.output'.format(multNodeLowArm), '{0}.input1'.format(multTranslateRevNode02))
        mc.connectAttr('{0}.output'.format(multTranslateRevNode02), '{0}.translateX'.format(inputList[2]))
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~ generate output
    return distanceNodeOne, distanceNodeTwo, controlDistNode



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ generate calc rig    
def create_calc_joints(inputNameList, inputObjectList, limb= 'arm', inputSubD= '2'):
    
    limbName= limb
    prefix= inputObjectList[0].split('_')[0]
    inputSubD= inputSubD + 1
    
    calcJnts= duplicate_joints(inputObjectList, inputNameList, .1)



    return calcJnts
    
#~~~~~~~~~~~~~~~~~~~~~~~~~ generate output joints
def create_output_joints(inputList, duplicateAmount, name= 'undefined'):

    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ create nodes
    
    #~~~~~~~~~~~~~~~~~~~~ modify duplicate amount
    dictnames= name
    #~~~~~~~~~~~~~~~~~~~~ modify duplicate amount
    divisionValue = duplicateAmount
    value = int(duplicateAmount)+ 1
    
    #~~~~~~~~~~~~~~~~~~~~ modify inputList amount
    allButFirstInput= inputList[1:]
    allButLastInput= inputList[:-1]
    
    #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ operation
    outputList= []
    
    jointTranslateValues= []
    for position, i in enumerate( allButFirstInput):
        
        tList= mc.xform(i , q= True , t= True)
        splicedValue = float(tList[0]) / value
        
        jointTranslateValues.append(splicedValue)
    
    for position, item in enumerate (allButLastInput):
        
        inBetweenJointList= dictBodyParts[dictnames][1::2]

        garbageList= change_strings_by_list(duplicateAmount, inputList[0], inBetweenJointList[position])
        
        objToParentUnder= inputList[position]
        
        for pos, i in enumerate(garbageList):
            exitJoint= duplicate_joints(inputList[position], i, 1)
            
            mc.parent(exitJoint, objToParentUnder)
            mc.xform(exitJoint, t= (jointTranslateValues[position], 0,0))
            
            objToParentUnder= exitJoint
        
        outputList.append(garbageList)
    outputList= [item for sublist in outputList for item in sublist]
    allOutputList= outputList[:]
    
    for pos, i in enumerate (inputList):
        allOutputList.insert(pos*value, inputList[pos])
    
    parentList= allOutputList[::]
    parentList.reverse()
    
    for pos, i in enumerate(parentList):
        try:
            mc.parent(parentList[pos], parentList[pos+1])
        except:
            pass
    
    return parentList
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ create nodes
def create_decimisation_nodes(inputName):
    """ documentation """
    
    nodeName= inputName
    nodeInput='{name}.input1'.format(name= nodeName)
    nodeOutput='{name}.output'.format(name= nodeName)
    
    
    cp_createNodeWithoutHistory('multDoubleLinear', n='{name}'.format(name= inputName), ss= True)
    mc.setAttr('{name}.input2'.format(name= nodeName), 0.1)
    mc.setAttr('{name}.input2'.format(name= nodeName), lock= True, keyable= False)
    
    return nodeName, nodeInput, nodeOutput

def create_divide_node(inputName, inputValue):
    """ documentation """
    
    nodeName= inputName
    indexValue= inputValue
    
    nodeInput='{name}.input1'.format(name= nodeName)
    nodeOutput='{name}.output'.format(name= nodeName)
    
    cp_createNodeWithoutHistory('multiplyDivide', n= '{name}'.format(name= nodeName), ss= True)
    mc.setAttr('{name}.operation'.format(name= nodeName), 2)
    mc.setAttr('{name}.input2X'.format(name= nodeName), indexValue)
    #mc.setAttr('{name}.input2X'.format(name= nodeName), lock= True, keyable= False)    
    
    return nodeName, nodeInput, nodeOutput



#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ blend Operations

def create_angle_blend_nodes(inputList):
    """ documentation """
    
    angleBlendNodes= []
    for i in inputList:
        
        splitupName= i.split('_')
        extractedName= splitupName[1]
        blendNodeName = format_object_name(change_strings_by_list(1, jointList[0], extractedName)[0], 'pbl', 'ikTOfk')
        
        cp_createNodeWithoutHistory('pairBlend', n='{name}'.format(name= blendNodeName), ss= True)
        mc.setAttr('{name}.rotInterpolation'.format(name= blendNodeName), 1)
        
        
        angleBlendNodes.append(blendNodeName)
    return angleBlendNodes


def create_blend_node(curveInput, nameInput, nodePos):
    """ documentation """

    newAttrName= 'ikFkBlend'
    nodeName= nameInput
    nodeOutput= '{name}.{attrN}'.format(name= nodeName, attrN= newAttrName)
    
    parentPosition= nodePos
    
    modem= mc.curve(p= curveInput, n= nodeName)
    
    
    newShapeName= format_shape_node(modem, cp_allowedPrefixes)

    
    axesList= ['x','y','z']
    for axes in axesList:
        transformList= ['t','r','s']
        for trans in transformList:
            mc.setAttr('{node}.{transform}{ending}'.format(node= nodeName, transform= trans, ending= axes), lock= True, keyable= False, channelBox= False)
    
    mc.setAttr('{node}.visibility'.format(node= nodeName), keyable= False, channelBox= False)
    mc.addAttr(modem, attributeType= 'float', longName= newAttrName, hasSoftMinValue= True, softMinValue= 0, hasSoftMaxValue= True, softMaxValue= 10, keyable= True)
    
    renamedOffset = format_object_name(nodeName,'srtOffset')    
    offsetNode = cp_createNodeWithoutHistory('transform', n= renamedOffset)
    mc.parent(modem, offsetNode)
    mc.xform(offsetNode, t= parentPosition)
        
    return nodeName, nodeOutput

####################################################################################
##########################          Connections         ############################
####################################################################################


        
def connect_pair_blend_nodes(listOne, listTwo, listThree, nodes, blendingInput, translate= False, outputList=[], limb= 'arm'):
    """ documentation """
    cp_limb= limb
    cp_limbCapitalized= cp_limb.capitalize()
    for position, i in enumerate(listOne):
        axisList= ['X','Y','Z']
        for axis in axisList:
            
            #~~~~~~~~~~~~~~~ rotation Connections
            
            mc.connectAttr('{name}.rotate{internalAxis}'.format(name= listOne[position], internalAxis= axis), '{name}.inRotate{internalAxis}1'.format(name= nodes[position], internalAxis= axis))
            mc.connectAttr('{name}.rotate{internalAxis}'.format(name= listTwo[position], internalAxis= axis), '{name}.inRotate{internalAxis}2'.format(name= nodes[position], internalAxis= axis))
            mc.connectAttr('{name}.outRotate{internalAxis}'.format(name= nodes[position], internalAxis= axis), '{name}.rotate{internalAxis}'.format(name= listThree[position], internalAxis= axis))


    #~~~~~~~~~~~~ translation Connections
        
    if translate == True:
        for position, i in enumerate(listOne):
            mc.connectAttr('{name}'.format(name= blendingInput), '{name}.weight'.format(name= nodes[position]))
            
            for axis in axisList:
                mc.connectAttr('{name}.translate{internalAxis}'.format(name= listOne[position], internalAxis= axis), '{name}.inTranslate{internalAxis}1'.format(name= nodes[position], internalAxis= axis))
                mc.connectAttr('{name}.translate{internalAxis}'.format(name= listTwo[position], internalAxis= axis), '{name}.inTranslate{internalAxis}2'.format(name= nodes[position], internalAxis= axis))
        
        if outputList == []:
            for axis in axisList:
                mc.connectAttr('{name}.outTranslate{internalAxis}'.format(name= nodes[position], internalAxis= axis), '{name}.translate{internalAxis}'.format(name= listThree[position], internalAxis= axis))

        else:
            outputList= outputList[::-1]
            middleItem = (len(outputList)+1)/2
            lastItem= len(outputList)+1
            halvedOutput= (len(outputList)-1)/2
            
            totalLimb= []
            totalLimb.append(outputList[1:middleItem])
            totalLimb.append(outputList[middleItem:lastItem])
            
            limbNames=['tUpper_INSERT_Split', 'tLower_INSERT_Split']
            
            
            for number, limbName in enumerate (limbNames):
                
                newNodeName= limbName.replace('_INSERT_', cp_limbCapitalized)                
                nodeOutput= create_divide_node(format_object_name(change_strings_by_list(1, jointList[0], newNodeName)[0], 'div'), halvedOutput)
                
                for axis in axisList:
                    mc.connectAttr('{name}.outTranslate{internalAxis}'.format(name= nodes[number+1], internalAxis= axis), '{name}{internalAxis}'.format(name= nodeOutput[1], internalAxis= axis))
                
                for connection in totalLimb[number]:
                    for axis in axisList:
                        mc.connectAttr('{name}{internalAxis}'.format(name= nodeOutput[2], internalAxis= axis), '{name}.translate{internalAxis}'.format(name= connection, internalAxis= axis))




def generate_matrix_blend(inputListOne, inputListTwo, inputListThree, inputBlenderNode, inputDecimationNode):
    """ documentation """
    prefix= inputListOne[0].split('_')[0]
    
    for pos, i in enumerate(inputListThree):
        
        middleName= inputListOne[pos].split('_')[1]+'_TO_'+inputListTwo[pos].split('_')[1]
        pairBlendName= '{0}_{1}_pBl'.format(prefix, middleName)
        decimName= inputDecimationNode[0]
        
        pairBlendNode= cp_createNodeWithoutHistory('pairBlend', n= pairBlendName)
        mc.setAttr('{name}.rotInterpolation'.format(name= pairBlendNode), 1, lock= True)
        
        mc.connectAttr('{0}.translate'.format(inputListOne[pos]), '{0}.inTranslate2'.format(pairBlendNode))
        mc.connectAttr('{0}.rotate'.format(inputListOne[pos]), '{0}.inRotate2'.format(pairBlendNode))
        
        mc.connectAttr('{0}.translate'.format(inputListTwo[pos]), '{0}.inTranslate1'.format(pairBlendNode))
        mc.connectAttr('{0}.rotate'.format(inputListTwo[pos]), '{0}.inRotate1'.format(pairBlendNode))
        
        mc.connectAttr('{0}'.format(inputDecimationNode[2]), '{0}.weight'.format(pairBlendNode))
        mc.connectAttr('{0}.outTranslate'.format(pairBlendNode), '{0}.translate'.format(inputListThree[pos]))
        mc.connectAttr('{0}.outRotate'.format(pairBlendNode), '{0}.rotate'.format(inputListThree[pos]))
        
        

#####################################################################################################################################################################################################################################################################
def connect_scalar_blend_divide(listOne, listTwo, listThree, nodes, blendingInput):
    """ connects two lists with another, bigger list, based on a specified amount """
    

    jointList= listThree
    divideAmount= (((len(jointList))-1)/2)
    listOne[0].rsplit('_')
    listTwo[0]
    
    
    dividesList= []
    for position, i in enumerate(listOne):
        sideString= i.rsplit('_', 1)
        newNodeName= '{name}_div'.format(name= sideString[0])
        create_divide_node(newNodeName, divideAmount)
        dividesList.append
    
    for position, i in enumerate(listOne):
        axisList= ['X','Y','Z']
        colorList= ['R','G','B']
        
        for pos, axis in enumerate(axisList):
            mc.connectAttr('{name}.translate{internalAxis}'.format(name= listOne[position], internalAxis= axis), '{name}.color1{internalColor}'.format(name= nodes[position], internalColor= colorList[pos]))
            mc.connectAttr('{name}.translate{internalAxis}'.format(name= listTwo[position], internalAxis= axis), '{name}.color2{internalColor}'.format(name= nodes[position], internalColor= colorList[pos]))
            
            #for 
            mc.connectAttr('{name}.output{internalColor}'.format(name= nodes[position], internalColor= colorList[pos]), '{name}.input{internalAxis}'.format(name= listThree[position], internalAxis= axis))
            
            mc.connectAttr('{name}.output{internalColor}'.format(name= nodes[position], internalColor= colorList[pos]), '{name}.translate{internalAxis}'.format(name= listThree[position], internalAxis= axis))
        
        mc.connectAttr('{name}'.format(name= blendingInput), '{name}.blender'.format(name= nodes[position]))

def connect_blend_to_decim(outputOne, inputTwo):
    mc.connectAttr('{name}'.format(name= outputOne),'{name}'.format(name= inputTwo))
    


####################################################################################
##########################          Executions         #############################
####################################################################################

def executeRig(limb= 'arm', inputSubD= 2):
    
    prefix= jointList[0].split('_')[0]
    cp_limb= limb
    
    #~~~~~~~~~~~~~~~~~ name creation
    
    ## CREATE NAME LISTS FOR JOINTS ##
    calcNames= format_list_name('joint', 'RIG', 'calc')
    fkNames= format_list_name('joint', 'RIG', 'fk')
    ikNames= format_list_name('joint', 'RIG', 'ik')
    
    atopNames= format_list_name('joint', 'jnt', 'atop')
    handName= format_list_name('joint', 'jnt', 'hand')
    
    ## CREATE BLEND NODE NAME ##
    blendNodeName = format_object_name(change_strings_by_list(1, jointList[0], cp_limb)[0], 'ctrl', 'Blender')
    decimation= create_decimisation_nodes(format_object_name(change_strings_by_list(1, jointList[0], '{limb}BlendDecim'.format(limb= cp_limb))[0], 'mult'))
    
    ## CREATE MASTER NAME
    masterNodeName = format_object_name(change_strings_by_list(1, jointList[0], cp_limb)[0], 'master')
    masterNode = create_master_node(masterNodeName, jointList)
    
    ###############    Construction Operations    #################
    blendMaster= create_blend_node(crossCurve, blendNodeName, (20, 5, 0))
    create_message_connections(masterNode, blendMaster[0])
    
    #create Shoulder Master joint
    atopShoulder= duplicate_joints(jointList[:1], atopNames[:1], .4)[0]
    
    '''
    smallJointPosition= calc_pv_pos(jointList, .03)
    mc.xform('pSphere1', t=smallJointPosition[0])
    mc.parentConstraint('Lf_calcShoulder_RIG', 'Lf_calcElbow_RIG', 'pSphere1', mo= True)
    '''
    
    #fk_rig_construction
    fkControlsCurves = create_fk_rig(fkNames, jointList, cp_limb)
    create_message_connections(masterNode, fkControlsCurves[1])

    addAndConnectJointsToMessage(fkControlsCurves[0][0], masterNode[3][0][1])
    addAndConnectJointsToMessage(fkControlsCurves[0][1], masterNode[3][1][1])
    addAndConnectJointsToMessage(fkControlsCurves[0][2], masterNode[3][2][1])

    set_override(fkControlsCurves[0])
    
    
    
    
    #ik_rig_construction
    ikControlsCurves = create_ik_rig(ikNames, jointList, cp_limb)
    create_message_connections(masterNode, ikControlsCurves[1])

    addAndConnectJointsToMessage(ikControlsCurves[0][0], masterNode[3][0][0])
    addAndConnectJointsToMessage(ikControlsCurves[0][1], masterNode[3][1][0])
    addAndConnectJointsToMessage(ikControlsCurves[0][2], masterNode[3][2][0])

    create_ik_stretch(ikControlsCurves[0], atopShoulder, blendMaster[0], masterNode, cp_limb)
    set_override(ikControlsCurves[0])
    
    #calc rig construction
    calcControlsCurves =create_calc_joints(calcNames, jointList, cp_limb, inputSubD)
    generate_matrix_blend(ikControlsCurves[0], fkControlsCurves[0], calcControlsCurves, blendMaster[1], decimation)
    set_override(calcControlsCurves)
    
    #output_joints_construction
    outputJntList= create_output_joints(jointList, inputSubD, cp_limb)
    #parent joints
    mc.parent(outputJntList[-1], atopShoulder)
    mc.parent(ikControlsCurves[0][0], atopShoulder)
    mc.parent(fkControlsCurves[0][0], atopShoulder)
    mc.parent(calcControlsCurves[0], atopShoulder)
    
    
    angularBlendNodes= create_angle_blend_nodes(jointList)
        
    ###############    Connection Operations    ###################
    
    connect_blend_to_decim(blendMaster[1], decimation[1])
    connect_pair_blend_nodes(fkNames, ikNames, jointList, angularBlendNodes, decimation[2], translate= True, outputList= outputJntList, limb= cp_limb)
    
    connect_visibility(masterNode[0], '{pref}_{limb}BlendDecim_mult'.format(pref= prefix, limb= cp_limb), '{pref}_{limb}BlendDecim_rev'.format(pref= prefix, limb= cp_limb))
    
    print("' ~ ALL GOOOD ~ '")