def getUParam( pnt = [], crv = None):

    point = OpenMaya.MPoint(pnt[0],pnt[1],pnt[2])
    curveFn = OpenMaya.MFnNurbsCurve(getDagPath(crv))
    paramUtill=OpenMaya.MScriptUtil()
    paramPtr=paramUtill.asDoublePtr()
    isOnCurve = curveFn.isPointOnCurve(point)
    if isOnCurve == True:
        
        curveFn.getParamAtPoint(point , paramPtr,0.001,OpenMaya.MSpace.kObject )
    else :
        point = curveFn.closestPoint(point,paramPtr,0.001,OpenMaya.MSpace.kObject)
        curveFn.getParamAtPoint(point , paramPtr,0.001,OpenMaya.MSpace.kObject )
    
    param = paramUtill.getDouble(paramPtr)  
    return param

def getDagPath( objectName):
    
    if isinstance(objectName, list)==True:
        oNodeList=[]
        for o in objectName:
            selectionList = OpenMaya.MSelectionList()
            selectionList.add(o)
            oNode = OpenMaya.MDagPath()
            selectionList.getDagPath(0, oNode)
            oNodeList.append(oNode)
        return oNodeList
    else:
        selectionList = OpenMaya.MSelectionList()
        selectionList.add(objectName)
        oNode = OpenMaya.MDagPath()
        selectionList.getDagPath(0, oNode)
        return oNode


def create_curve():
    import maya.cmds as mc
    vtx = mc.ls( fl = 1, os= True)
    positionList= []
    for v in vtx:
        pos= cmds.xform(v, q = 1, ws = 1, t=1)
        positionList.append(pos)
    mc.curve(n = 'curve',p= positionList, d= 1)

def main(side, name, direction):
    from maya import cmds, OpenMaya
    
    
    if (cmds.selectPref(tso=True, q=True)== 0):
        cmds.selectPref(tso=True)
    
    
    nameCap= name.capitalize()
    
    center = "{0}_eyeBall_jnt".format(side)
    upVecPos= "{0}_eyeballUpVec_pos".format(side)

    
    vtx = cmds.ls( fl = 1, os= True)
    positionList= []
    startJointList= []
    endJointsList= []
    locatorList= []
    




    for iteration, v in enumerate(vtx):

        numberation= str(iteration+1).zfill(len(str(len(vtx))))


        
        cmds.select(cl = 1)
        jnt = cmds.joint(n= '{0}_{1}{2}Boarder_{3}_jnt'.format(side, direction, nameCap, numberation), radius= 0.1)
        pos= cmds.xform(v, q = 1, ws = 1, t=1)
        cmds.xform(jnt, ws = 1, t = pos)
        
        posC = cmds.xform(center, q = 1, ws = 1, t = 1)
        cmds.select (cl = 1)
        jntC = cmds.joint(n= '{0}_{1}{2}Center_{3}_RIG'.format(side, direction, nameCap, numberation), radius= 0.1)
        cmds.xform(jntC, ws =1, t = posC)
        cmds.parent(jnt, jntC)
        
        cmds.joint(jntC, e = 1, oj= "xyz", secondaryAxisOrient = "yup", ch = 1, zso = 1)

        startJointList.append(jntC)
        positionList.append(pos)        
        endJointsList.append(jnt)
    



    for iteration, eJ in enumerate(endJointsList):

        numberation= str(iteration+1).zfill(len(str(len(vtx))))




        loc= cmds.spaceLocator(n= '{0}_{1}{2}Boarder_{3}_pos'.format(side, direction, nameCap, numberation))[0]
        pos= cmds.xform(eJ, q= True, ws= True, t= True)
        cmds.xform(loc, ws= True, t= pos)
        par= cmds.listRelatives(eJ,p= True)[0]
        cmds.aimConstraint(loc, par, mo= True, weight= 1, aimVector= (1,0,0), upVector= (0,1,0), worldUpType= 'object', worldUpObject= upVecPos)
        
        
        locatorList.append(loc)

    curveHighNode= cmds.curve(n = '{0}_{1}{2}HighCurve_crv'.format(side, direction, nameCap),p= positionList, d= 1)
    shapeHighNode= cmds.listRelatives(curveHighNode, shapes= True)[0]





    for iteration, p in enumerate(positionList):
        print 'i believe this is done'
        numberation= str(iteration+1).zfill(len(str(len(vtx))))



        u= getUParam(p, shapeHighNode)
        pci= cmds.createNode('pointOnCurveInfo', n= '{0}_{1}{2}Position_{3}_pCi'.format(side, direction, nameCap, numberation))
        cmds.connectAttr('{0}.worldSpace'.format(shapeHighNode), '{0}.inputCurve'.format(pci))
        cmds.setAttr('{0}.parameter'.format(pci), u)
        cmds.connectAttr('{0}.position'.format(pci), '{0}.translate'.format(locatorList[iteration]))

    curveLowNode= cmds.curve(n = '{0}_{1}{2}LowCurve_crv'.format(side, direction, nameCap),p= positionList, d= 1)
    cmds.rebuildCurve(curveLowNode, rt=0, s=4 )
    #cmds.delete('{0}.cv[1]'.format(curveLowNode), '{0}.cv[5]'.format(curveLowNode))
    
    locGroup= cmds.group(locatorList, n= '{0}_{1}{2}LowLocs_grp'.format(side, direction, nameCap))
    jointGroup= cmds.group(startJointList, n= '{0}_{1}{2}LowJoints_grp'.format(side, direction, nameCap))

    
main()
