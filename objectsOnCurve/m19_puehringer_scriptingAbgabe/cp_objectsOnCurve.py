###########################################################################################################################################
#                                                                                                                                         #
#                                                    object on curve constraint script  v0.85                                             #
#                                                              Christof Puehringer                                                        #
#                                                                                                                                         #
#                                                                                                                                         #
#                                                                   TESTBUILD                                                             #
#                                                                                                                                         #
###########################################################################################################################################

#TO DO=====================================================================================================================================


    #create extra Button for Toggle LRA fur das:: get objects into a list
    #umbenennen der erstellten Objekte
    #nummerierung der erstellten Objekte
    #ausgrauen der UI
    #tooltip fixen
    

#==========================================================================================================================================

import maya.cmds as cmds

import maya.cmds as mc
import math
import random



def main():
    import sys
    import os.path
    import maya.cmds as mc
    
    uiFilePath= os.path.normpath("C:\\work\\cp_maya_scripts\\cp_maya_scripts\\objectsOnCurve\\cp_objectsOnCurve.ui")
    if mc.window('cp_objectsOnCurveWindow', exists= True):
        mc.deleteUI('cp_objectsOnCurveWindow')
    
    window= mc.loadUI(uiFile= uiFilePath)
    mc.showWindow(window)


def cp_createStuff(*args):
    #get Selection
    sel = mc.ls(sl = True)
    shapes = mc.listRelatives(sel[0], shapes = True)
    
    #Query the UI
    #object Type Query
    
    #cP_objectCheck = mc.textField('cP_object', query= True, text= True)
    
    
    #number Query
    
    cP_pointsCheck= queryFieldInteger('cP_points')
    
    #options Query
    
    cP_moPathCheck = mc.checkBox('cp_motionPath_4', query= True, value= True)
    cP_loopCheck = mc.checkBox('cp_loopingPath_4', query= True, value= True)
    
    cP_offsetCheck = mc.checkBox('cp_offsetGroup', query= True, value= True)
    
    #aimOptions Query
    cP_aimType = mc.optionMenu('cp_aimTypeSelect_4', query= True, value= True)
    
    
    listCheck = queryObjList()
    
    #aimType Query
    if cP_aimType == 'locator':
        enumAimType = int(1)
    elif cP_aimType == 'curve':
        enumAimType = int(2)
    elif cP_aimType == 'world':
        enumAimType = int(3)
    else:
        enumAimType = int(0)
    
    #cP_upVecCrv = mc.intFieldGrp('upVecCurve', query= True, value= True)
    if enumAimType == 2:
        xUpVec= queryFieldInteger('cp_upVecCurve_xValue_4', False)
        yUpVec= queryFieldInteger('cp_upVecCurve_yValue_4', False)
        zUpVec= queryFieldInteger('cp_upVecCurve_zValue_4', False)
        cP_upVecCrv= (xUpVec, yUpVec, zUpVec)
    else:
        cP_upVecCrv= (0,0,0)

    


#declaration of Variables 
#==========================================================================================================================================
    
    #normalized Position on Curve
    if cP_pointsCheck > 1:
        normInbetween = 1.0 / (cP_pointsCheck - 1)
    else:
        normInbetween = 0.0
    
    
    #locator Curve Shape
    cP_locCrvShape = [(0.0, -4.00000001713401, 0.0), (0.0, -1.7134009766550662e-08, 1.0), (0.0, 1.9999999828659871, 0.0), (0.0, -1.7134009766550662e-08, -1.0), (0.0, -4.00000001713401, 0.0), (1.0, -1.7134009766550662e-08, 0.0), (0.0, 1.9999999828659871, 0.0), (-1.0, -1.7134009766550662e-08, 0.0), (0.0, -4.00000001713401, 0.0), (0.0, -1.7134009766550662e-08, 0.0), (0.0, -1.7134009766550662e-08, 1.0), (0.0, -1.7134009766550662e-08, 0.0), (-1.0, -1.7134009766550662e-08, 0.0), (0.0, -1.7134009766550662e-08, 0.0), (0.0, -1.7134009766550662e-08, -1.0), (0.0, -1.7134009766550662e-08, 0.0), (1.0, -1.7134009766550662e-08, 0.0), (0.0, -1.7134009766550662e-08, 0.0), (0.0, 1.9999999828659871, 0.0), (0.0, -1.7134009766550662e-08, 0.0), (0.0, -1.7134009766550662e-08, 0.0), (0.0, -1.7134009766550662e-08, 0.0), (0.0, -1.7134009766550662e-08, 0.0)]
    cP_locCrvAmount= (len(cP_locCrvShape))-1
    
    
    
    #object inherits transform
    
    cP_inheritance = False
    
    #objectTypes to Endstring
    
    
    #mopathControl Curve Shape
    cP_mopathCrvShape = [(-3.0, 0.0, 1.0), (-4.0, 0.0, 0.0), (-3.0, 0.0, -1.0), (-2.0, 0.0, -1.0), (-1.0, 0.0, -2.0), (-1.0, 0.0, -3.0), (0.0, 0.0, -4.0), (1.0, 0.0, -3.0), (1.0, 0.0, -2.0), (2.0, 0.0, -1.0), (3.0, 0.0, -1.0), (4.0, 0.0, 0.0), (3.0, 0.0, 1.0), (2.0, 0.0, 1.0), (1.0, 0.0, 2.0), (1.0, 0.0, 3.0), (0.0, 0.0, 4.0), (-1.0, 0.0, 3.0), (-1.0, 0.0, 2.0), (-2.0, 0.0, 1.0), (-3.0, 0.0, 1.0), (-2.0, 0.0, 1.0), (-2.0, 0.0, 0.0), (-2.0, 0.0, -1.0), (-1.0, 0.0, -2.0), (0.0, 0.0, -2.0), (1.0, 0.0, -2.0), (2.0, 0.0, -1.0), (2.0, 0.0, 0.0), (2.0, 0.0, 1.0), (1.0, 0.0, 2.0), (0.0, 0.0, 2.0), (-1.0, 0.0, 2.0), (-2.0, 0.0, 1.0), (-3.0, 0.0, -1.0), (-3.0, 0.0, 1.0), (-2.0, 0.0, -1.0), (-1.0, 0.0, -2.0), (1.0, 0.0, -3.0), (-1.0, 0.0, -3.0), (1.0, 0.0, -2.0), (2.0, 0.0, -1.0), (3.0, 0.0, 1.0), (3.0, 0.0, -1.0), (2.0, 0.0, 1.0), (1.0, 0.0, 2.0), (-1.0, 0.0, 3.0), (1.0, 0.0, 3.0), (-1.0, 0.0, 2.0), (1.0, 0.0, 2.0)]
    
    
#Execution   
#==========================================================================================================================================    
    #Loop Through
    for selected in sel:
        curveAmounts = 0
        for s in shapes:
            shapeTypeCheck = mc.objectType(mc.listRelatives(selected, shapes= True)[0], isType = 'nurbsCurve' )

            cp_objectSizeToJointSize= cp_getBoundingBoxSize(s)
            if shapeTypeCheck == 1:
                
                #create List for Group to parent under
                objectsAttatched= []
                
                #other Stuff
                heyda = str(sel[curveAmounts])+ '_01_pOCi'
                selectionNumber = str(len(sel))
                length = mc.arclen(s)
                lengthInbetween = length * normInbetween
                rotationOfCurve = mc.xform(sel[curveAmounts],q=1,ws=1,ro=1)
                positionOfCurve = mc.xform(sel[curveAmounts],q=1,ws=1,rp=1)
                
                
                #create Aim-Locator ASK
                if enumAimType == 1:
                    aimLoc= mc.createNode('locator',n= selected +'_aimPoint_locShape', ss = True)
                    cP_aimShape= mc.curve(n= selected +'_aimPoint_crv', d=1, p= cP_locCrvShape)
                    cP_aimLocShape = mc.listRelatives(str(cP_aimShape),s=1)
                    mc.rename( str(cP_aimLocShape[0]), cP_aimShape +'Shape')
                    
                    def cP_setAttr(inputVar):
                        operatedObject = inputVar.rstrip('Shape')
                        mc.setAttr(operatedObject +'.translateX', positionOfCurve[0])
                        mc.setAttr(operatedObject +'.translateY', positionOfCurve[1])
                        mc.setAttr(operatedObject +'.translateZ', positionOfCurve[2])
                        mc.setAttr(operatedObject +'.rotateX', rotationOfCurve[0])
                        mc.setAttr(operatedObject +'.rotateY', rotationOfCurve[1])
                        mc.setAttr(operatedObject +'.rotateZ', rotationOfCurve[2])
                   
                    cP_setAttr(aimLoc)
                    mc.setAttr(aimLoc +'.worldPosition[0].worldPositionX', 0)
                    mc.setAttr(aimLoc +'.hiddenInOutliner', True)
                    
                    mc.setAttr(str(cP_aimShape) +'.overrideEnabled', True)
                    mc.setAttr(str(cP_aimShape) +'.overrideRGBColors', True)
                    mc.setAttr(str(cP_aimShape) +'.overrideColorRGB', 1,.98,.3)                    
                    
                    mc.parent(cP_aimShape +'Shape', str(aimLoc.rstrip('Shape')), relative= True, shape= True)
                    mc.group(str(aimLoc), name= selected+'_aimPoint_srtOffset', absolute= True)
                    mc.delete(cP_aimShape)
                   
                #create Offset Curve
                if enumAimType == 2:
                    OffsetCurveName = selected +'_aimCrv'
                    mc.offsetCurve( selected, cl=True, cr=2.0, distance= cp_objectSizeToJointSize, nr= cP_upVecCrv, constructionHistory = False, n= OffsetCurveName)
                    
                    mc.setAttr(str(OffsetCurveName) +'.overrideEnabled', True)
                    mc.setAttr(str(OffsetCurveName) +'.overrideRGBColors', True)
                    mc.setAttr(str(OffsetCurveName) +'.overrideColorRGB', .7,.6,.1)
                    
                    aimCrvShape = mc.listRelatives(OffsetCurveName, shapes = True)
                
                    aimCrvShapeString = "','".join(map(str, aimCrvShape))
                    
                    
                    
#FUNCTIONS---------------------------------------------------------------------------------------------------------------------------------                   
                
                #createNode Function
                def cP_create(type, name):
                    inputNode= mc.createNode(type,n=name, ss = True)
                    mc.setAttr('{0}.isHistoricallyInteresting'.format(name), 0)
                    return inputNode
                    
                #connectNode Functions
                def cP_connect(fromNode, fromAttr, inNode, inAttr):
                    mc.connectAttr( fromNode +'.'+ fromAttr, inNode +'.'+ inAttr)
                
                xyzList= ['x', 'y', 'z']
                
                def cP_connectV(fromNode, fromAttr, inNode, inAttr):
                    for axis in xyzList:
                        mc.connectAttr ('{0}.{1}{2}'.format(fromNode, fromAttr, axis.capitalize()), '{0}.{1}{2}'.format(inNode, inAttr, axis.capitalize()))
                    
                def cP_connect4x4Mtx (fromNode, fromAttr, inNode,inAttr):
                    for index, axis in enumerate(xyzList):
                        mc.connectAttr ('{0}.{1}{2}'.format(fromNode, fromAttr, axis.capitalize()), '{0}.in{1}{2}'.format(inNode, inAttr, str(index)))
                    
                def cP_connectPMA (fromNodeOne, fromAttrOne, fromNodeTwo, fromAttrTwo, inNode, toNode, toAttr):
                   
                   #incoming Connections
                   #LOOP THIS FOR THE FUTURE
                   mc.connectAttr (fromNodeOne +'.'+ fromAttrOne +'X',inNode +'.input3D[0].input3Dx')
                   mc.connectAttr (fromNodeOne +'.'+ fromAttrOne +'Y',inNode +'.input3D[0].input3Dy')
                   mc.connectAttr (fromNodeOne +'.'+ fromAttrOne +'Z',inNode +'.input3D[0].input3Dz')
                   
                   #LOOP THIS FOR THE FUTURE
                   mc.connectAttr (fromNodeTwo +'.'+ fromAttrTwo +'X',inNode +'.input3D[1].input3Dx')
                   mc.connectAttr (fromNodeTwo +'.'+ fromAttrTwo +'Y',inNode +'.input3D[1].input3Dy')
                   mc.connectAttr (fromNodeTwo +'.'+ fromAttrTwo +'Z',inNode +'.input3D[1].input3Dz')
                   
                   #outgoing Connections
                   #LOOP THIS FOR THE FUTURE
                   mc.connectAttr (inNode +'.output3D.output3Dx',toNode +'.in'+ toAttr +'0')
                   mc.connectAttr (inNode +'.output3D.output3Dy',toNode +'.in'+ toAttr +'1')
                   mc.connectAttr (inNode +'.output3D.output3Dz',toNode +'.in'+ toAttr +'2')
                   
                #create MoPath Loop   
                def cP_setKeyCycle(driver, drivingAttr, receiver, receivingAttr):
        
                    inComing = str(driver) +'.'+ str(drivingAttr)
                    outGoing = str(receiver) +'.'+ str(receivingAttr)
                    
                    mc.setDrivenKeyframe(outGoing, cd=inComing, dv= 0, v=0, itt= 'linear', ott= 'linear')
                    mc.setDrivenKeyframe(outGoing, cd=inComing, dv= 1, v=1, itt= 'linear', ott= 'linear')
                    
                    nodeName = outGoing.replace('.','_')
                    mc.setAttr (nodeName +'.preInfinity', 3)
                    mc.setAttr (nodeName +'.postInfinity', 3)               
                
                positionOnCurve = 0.00
                
#CREATION----------------------------------------------------------------------------------------------------------------------------------          
  
                #create MoPath-Control:
                if cP_moPathCheck == True:
                    moPathCtrl= selected+ '_moptath_ctrl'
                    cP_curveName = mc.curve(n= moPathCtrl,p= cP_mopathCrvShape, d=1)
                    
                    cP_curveShape= mc.listRelatives(str(cP_curveName), s=1)
                    mc.rename(str(cP_curveShape[0]), cP_curveName+"Shape")
                    
                    mc.setAttr(str(moPathCtrl) +'.overrideEnabled', 1)
                    
                    mc.setAttr(str(moPathCtrl) +'.overrideEnabled', True)
                    mc.setAttr(str(moPathCtrl) +'.overrideRGBColors', True)
                    mc.setAttr(str(moPathCtrl) +'.overrideColorRGB', 1,.98,.3)
                    
                    if not cP_loopCheck:
                        mc.addAttr(str(moPathCtrl), shortName ='mPP', longName='moPathProgress', attributeType='double', k= True, w= True, hasSoftMinValue= True, softMinValue= -10, hasSoftMaxValue= True, softMaxValue= 10)
                    else:
                        mc.addAttr(str(moPathCtrl), shortName ='mPP', longName='moPathProgress', attributeType='double', k= True, w= True)
                        
                #loopThrough Function:
                for x in range(0, cP_pointsCheck):
                    
                    #set Variables MAKE BETTER
                    number = str(x).zfill(len(str(cP_pointsCheck)))
                    
                    pOCi = selected +'_'+ number +'_pOCi'
                    kFname = pOCi + '_parameter'
                    
                    ffMtx = selected +'_'+ number +'_ffMtx'
                    dcMtx = selected +'_'+ number +'_dcMtx'
                    objectToAttatch = selected +'_'+ number +'_jnt'
                    aim_pOCi = selected +'_aimCrv_'+ number +'_pOCi'
                    vecSub = selected +'_aimCrv_'+ number +'_vecSub'
                    
                    
                    rtOffset = selected +'_'+ number +'_rtOffset' 
                    
                    moPathAdd = selected +'_moPath_'+ number +'_add'
                    moPathRun = selected +'_moPath_'+ number +'_aCUU'
                    moPathEase = selected +'_moPath_'+ number +'_div'
                    
                    
                    
                    #create Component Nodes
                    cP_create ('pointOnCurveInfo',pOCi)
                    mc.setAttr(pOCi +'.parameter', positionOnCurve)
                    mc.setAttr(pOCi +'.turnOnPercentage',True)
                    
                    cP_create ('fourByFourMatrix',ffMtx)
                    cP_create ('decomposeMatrix', dcMtx)
                    
                    
                    
                    if listCheck:
                        objectToAttatch = objectToAttatch.replace('_jnt', '_geo')
                        if len(listCheck) == 1:
                            cP_attatchingObject= mc.duplicate(listCheck[0], n= objectToAttatch)
                            
                        else:
                            randomObjIndex= random.randint(0,len(listCheck)-1)
                            cP_attatchingObject= mc.duplicate(listCheck[randomObjIndex], n= objectToAttatch)
                            
                    else:
                        cP_attatchingObject = cP_create ('joint', objectToAttatch)
                        mc.setAttr('{0}.radius'.format(cP_attatchingObject), cp_objectSizeToJointSize)
                    #cP_attatchingObject = cP_create ('transform', jnt)
                    
                    
                    
                    #create Offset Node
                    if cP_offsetCheck == True:
                        cP_create('transform', rtOffset)
                        mc.parent(objectToAttatch, rtOffset, relative= True)
                        
                        objectsAttatched.append(rtOffset)
                    
                    else:
                        objectsAttatched.append(objectToAttatch)
                    
                    #if cP_inheritance == False:
                    #mc.setAttr(cP_attatchingObject +'.inheritsTransform', False)
                    
                    
                    #create MotionPath
                    if cP_moPathCheck == True:
                        cP_create('addDoubleLinear', moPathAdd)
                        mc.setAttr(moPathAdd +'.input1', positionOnCurve)
                        
                        cP_create('multDoubleLinear', moPathEase)
                        mc.setAttr(moPathEase +'.input1', 0.1)
                        
                        
                    #create Aim-Curve
                    if enumAimType == 2:
                        cP_create ('pointOnCurveInfo',aim_pOCi)
                        mc.setAttr(aim_pOCi +'.parameter', positionOnCurve)
                        mc.setAttr(aim_pOCi +'.turnOnPercentage',True)
                                                
                    if enumAimType != 0:
                        cP_create ('plusMinusAverage', vecSub)
                        mc.setAttr(vecSub +'.operation', 2)
                        mc.setAttr(vecSub +'.input3D[0]'+ '.input3Dx', 0)
                        mc.setAttr(vecSub +'.input3D[1]'+ '.input3Dx', 5)
                    
#CONNECTION--------------------------------------------------------------------------------------------------------------------------------
                    
                    #connect Main-Curve
                    cP_connect (s, 'worldSpace[0]', pOCi, 'inputCurve')
                    
                    if cP_moPathCheck == True:
                        
                        cP_connect(moPathCtrl, 'moPathProgress', moPathEase, 'input2')
                        cP_connect(moPathEase,'output', moPathAdd, 'input2')
                        
                        if cP_loopCheck == True:
                            
                            cP_setKeyCycle(moPathAdd, 'output', pOCi, 'parameter')
                        else:
                            cP_connect(moPathAdd, 'output', pOCi, 'parameter')
                            
                    cP_connect4x4Mtx (pOCi, 'position', ffMtx, '3')
                    if enumAimType != 3:
                        cP_connect4x4Mtx (pOCi, 'normalizedTangent', ffMtx, '0')
                        
                    cP_connect (ffMtx, 'output', dcMtx, 'inputMatrix')
                    
                    if cP_offsetCheck == True:
                        cP_connectV (dcMtx, 'outputRotate', rtOffset, 'rotate')
                        cP_connectV (dcMtx, 'outputTranslate', rtOffset, 'translate')
                    else:
                        cP_connectV (dcMtx, 'outputRotate', objectToAttatch, 'rotate')
                        cP_connectV (dcMtx, 'outputTranslate', objectToAttatch, 'translate')
                    
                    #connect Main-Curve to Aim-Locator
                    if enumAimType == 1:
                        cP_connectPMA (aimLoc, 'worldPosition[0].worldPosition', pOCi, 'position', vecSub, ffMtx, '1')
                        
                    #connect Main-Curve to Aim-Curve
                    if enumAimType == 2:
                        if cP_loopCheck == True:
                            cP_connect(kFname, 'output',aim_pOCi, 'parameter')

                        cP_connect (aimCrvShapeString, 'worldSpace[0]', aim_pOCi, 'inputCurve')
                        cP_connectPMA (aim_pOCi, 'position', pOCi, 'position', vecSub, ffMtx, '1')
                                        
                    #iterate Position
                    curveAmounts += 1
                    
                    positionOnCurve += normInbetween
                    

#RESULT------------------------------------------------------------------------------------------------------------------------------------

                #issue positive Result Text
                if enumAimType == 0:
                    hookupTypeString = 'nothing'
                elif enumAimType == 1:
                    hookupTypeString = 'one locator'
                else:
                    hookupTypeString = 'one curve'
                    
                if len(sel) == 1:
                    resultMessage ='"'+ selectionNumber +' curve has been hooked up to '+ hookupTypeString +'"'
                else:
                    if enumAimType == 0:
                        resultMessage ='"'+ selectionNumber +' curves have been hooked up to '+ hookupTypeString +'"'
                        
                    else:
                        resultMessage ='"'+ selectionNumber +' curves have been hooked up to '+ selectionNumber +' '+ hookupTypeString.lstrip('one ') + 's"'
                
                result = mc.confirmDialog(
                title='operation finished', ma= 'center', bgc= (0.01,0.4,0.05),
                message= resultMessage,
                button=['OK'],
                defaultButton='OK')
                
            else:
                #issue negative Result Text
                result = mc.confirmDialog(
                title='please select a curve', ma= 'center',bgc= (0.4,0.05,0.1),
                message='"this was no curve, my little dumdum"',
                button=['OK'],
                defaultButton='OK'
                )
                
            #fancy Locator Control
            if enumAimType == 1:
                
                #create Nodes for aimPoint
                cP_aim_nPOC= mc.createNode('nearestPointOnCurve',n= selected +'_aimLoc_nPOC', ss = True)
                                
                cP_aim_dMtx01= mc.createNode('decomposeMatrix',n= selected +'_aimPos_dMtx', ss = True)
                cP_aim_mMtx01= mc.createNode('multMatrix',n= selected +'_aimPos_mMtx', ss = True)
                
                cP_aim_dMtx02= mc.createNode('decomposeMatrix',n= selected +'_aimLoc_dMtx', ss = True)
                cP_aim_mMtx02= mc.createNode('multMatrix',n= selected +'_aimLoc_mMtx', ss = True)
                
                cP_aim_locShape= mc.createNode('locator',n= selected +'_aimLoc_posLocShape', ss = True)
                cP_aim_locTransform= selected +'_aimLoc_posLocShape'.rstrip('Shape')
                mc.setAttr(cP_aim_locTransform +'.visibility', 0)            
               
                #hook up Nodes for aimPoint
                def cP_vConnect(inNode, inAttr, outNode, outAttr):
                    mc.connectAttr(inNode +'.'+ inAttr +'X', outNode +'.'+ outAttr +'X')
                    mc.connectAttr(inNode +'.'+ inAttr +'Y', outNode +'.'+ outAttr +'Y')
                    mc.connectAttr(inNode +'.'+ inAttr +'Z', outNode +'.'+ outAttr +'Z')
                
                mc.connectAttr(aimLoc.rstrip('Shape') +'.worldMatrix[0]', str(cP_aim_mMtx01) +'.matrixIn[0]')
                
                mc.connectAttr(str(selected+'_aimPoint_srtOffset') +'.parentInverseMatrix[0]', str(cP_aim_mMtx01) +'.matrixIn[1]')
                
                mc.connectAttr(str(cP_aim_mMtx01) +'.matrixSum', str(cP_aim_dMtx01) +'.inputMatrix')
                
                cP_vConnect(str(cP_aim_dMtx01), 'outputTranslate', str(cP_aim_nPOC), 'inPosition' )
                
                
                mc.connectAttr(selected +'.worldSpace[0]', str(cP_aim_nPOC) +'.inputCurve')
                
                cP_vConnect (str(cP_aim_nPOC),'position', str(cP_aim_locTransform), 'translate')
                
                mc.connectAttr(str(cP_aim_locTransform)+'.worldMatrix[0]', str(cP_aim_mMtx02) +'.matrixIn[0]')
                mc.connectAttr(aimLoc.rstrip('Shape') +'.worldInverseMatrix[0]', str(cP_aim_mMtx02) +'.matrixIn[1]')
                
                mc.connectAttr(cP_aim_mMtx02 +'.matrixSum', cP_aim_dMtx02 +'.inputMatrix')
                
                mc.connectAttr(str(cP_aim_dMtx02)+ '.outputTranslateX', str(selected +'_aimPoint_crvShape') +'.controlPoints['+ str(cP_locCrvAmount) +'].xValue')
                mc.connectAttr(str(cP_aim_dMtx02)+ '.outputTranslateY', str(selected +'_aimPoint_crvShape') +'.controlPoints['+ str(cP_locCrvAmount) +'].yValue')
                mc.connectAttr(str(cP_aim_dMtx02)+ '.outputTranslateZ', str(selected +'_aimPoint_crvShape') +'.controlPoints['+ str(cP_locCrvAmount) +'].zValue')
                
                mc.setAttr (cP_aim_locTransform +'.hiddenInOutliner', True)
        
        masterNodeName= 'mopaths_connectedItems_master'
        if not mc.objExists(masterNodeName):
            nodeMaster= cP_create('transform', masterNodeName)
            mc.addAttr(nodeMaster, shortName='ci', longName= 'createdItems', attributeType= 'message')
            mc.setAttr('{0}.hiddenInOutliner'.format(nodeMaster), True)
            
        for i in objectsAttatched:
            mc.addAttr(i, shortName='imp', longName= 'isMoPath', attributeType= 'message')
            mc.connectAttr( '{0}.createdItems'.format(masterNodeName),'{0}.isMoPath'.format(i))
        
        cP_resultGroup= mc.group(objectsAttatched, n=selected +'_oP_grp', world= True)

def cp_placeStuff(*args):
    
    #QUERY
    #get Selection
    sel = mc.ls(sl = True)
    shapes = mc.listRelatives(sel[0], shapes = True)
    
    #Query the UI
    cP_pointsCheck= queryFieldInteger('cP_placerPoints')
    
    cP_offsetCheck = mc.checkBox('cp_offsetGroup', query= True, value= True)
    
    cp_scaleOffset= queryFieldFloat('cP_scaleOffset', False)
    print type(cp_scaleOffset)
    cp_positionOffset= queryFieldInteger('cP_posOffset', False)
    
    if cP_pointsCheck > 1:
        normInbetween = 1.0 / (cP_pointsCheck - 1)
    else:
        normInbetween = 0.0

    listCheck = queryObjList()

    #OPERATION
    for s in sel:
        if mc.objectType(mc.listRelatives(s, s= True), isType='nurbsCurve'):
            curvePoint= 0
            
            didgets= len(str(cP_pointsCheck))
            createdItems= []
            for i in range(0,cP_pointsCheck):
                nameJnt= '{0}_{1}_{2}'.format(s, str(i).zfill(didgets), 'jnt')
                placementPosition= tuple(mc.pointOnCurve(s, pr= curvePoint, top= True))
                randomObjIndex= random.randint(0,len(listCheck)-1)
                
                
                if listCheck:
                    nameObj= nameJnt.replace('_jnt','_geo')
                    if len(listCheck) == 1:
                        objectToAttatch= mc.duplicate(listCheck[0], n= nameObj)[0]
                        
                    else:
                        objectToAttatch= mc.duplicate(listCheck[randomObjIndex], n= nameObj)[0]
                        

                else:
                    objectToAttatch= mc.createNode('joint', n='joint_'+ str(i))
                    
                if cP_offsetCheck:
                    nameOffset= nameJnt.replace('_jnt', '_rtOffset')
                    nodeOffset= mc.createNode('transform', n= nameOffset)
                    mc.parent(objectToAttatch, nodeOffset)
                    objectToAttatch= nameOffset
                    
                    
                mc.xform(objectToAttatch, t= placementPosition)
                
                
                if listCheck:
                    print cp_scaleOffset
                    if cp_scaleOffset != 0:

                        if len(listCheck) == 1:
                            currentScaling= mc.xform( listCheck[0], s= True, q= True, r= True)
                            
                        else:
                            currentScaling= mc.xform( listCheck[randomObjIndex], s= True, q= True, r= True)

                        randomScaling= random.uniform(-1*cp_scaleOffset, cp_scaleOffset)
                        finalScaling = (currentScaling[0] + randomScaling, currentScaling[1] + randomScaling, currentScaling[2] + randomScaling)
                        mc.xform(objectToAttatch, s= finalScaling)
                

                
                
                createdItems.append(objectToAttatch)
                
                curvePoint= curvePoint + normInbetween
            mc.group(createdItems, n= s +'_oP_grp', world= True)



def cp_getBoundingBoxSize(inputMesh):
    import maya.cmds as mc
    minBBox= mc.getAttr('{0}.boundingBoxMin'.format(inputMesh))
    maxBBox= mc.getAttr('{0}.boundingBoxMax'.format(inputMesh))
    xCombined= abs(minBBox[0][0]) + abs(maxBBox[0][0])
    yCombined= abs(minBBox[0][1]) + abs(maxBBox[0][1])
    zCombined= abs(minBBox[0][2]) + abs(maxBBox[0][2])
    
    allRelative= ((xCombined/ 2)+(yCombined/ 2)+(zCombined/ 2)/ 3)* .05
    return allRelative
    

def queryFieldInteger(input, errormessage= True):
    import maya.cmds as mc
    
    output= mc.textField(input, q= True, text= True)
    if output.isdecimal():
        output= int(output)
        
        if errormessage:
            if not output:
                mc.confirmDialog( title='Confirm', message='please select not zero', button=['i promise'], defaultButton='Yes')
                output= 0
    else:
        mc.confirmDialog( title='Confirm', message='please select a integer', button=['i promise'], defaultButton='Yes')
        output= 0
    
    return output


def queryFieldFloat(input, errormessage= True):
    import maya.cmds as mc
    
    output= mc.textField(input, q= True, text= True)
    outputCheck= (output.replace('.', ''))

    if outputCheck.isnumeric():
        print 'is numeric'
        output= float(output)
        
        if errormessage:
            if not output:
                mc.confirmDialog( title='Confirm', message='please select not zero', button=['i promise'], defaultButton='Yes')
                output= 0
    else:
        if errormessage:
            mc.confirmDialog( title='Confirm', message='please select a integer', button=['i promise'], defaultButton='Yes')
        output= 0
    
    print type(output)
    return output


def fillObjList():
    import maya.cmds as mc
    
    selectionList= mc.ls(sl= True)
    checkerList=[]
    selectedItems= mc.textScrollList('cp_oocInputList', query= True, allItems= True)
    
    if selectedItems != None:
        selectionList= [x for x in selectionList if x not in selectedItems]
    else:
        pass

    mc.textScrollList('cp_oocInputList', edit= True, append= selectionList)

def removeObjList():
    import maya.cmds as mc
    selectedItems= mc.textScrollList('cp_oocInputList', query= True, selectItem= True)
    if selectedItems != None:
        for i in selectedItems:
            mc.textScrollList('cp_oocInputList', edit= True, removeItem= i)


def removeAll():
    mc.textScrollList('cp_oocInputList', edit= True, removeAll= True)


def cp_delete_ui():
    mc.deleteUI('cp_objectsOnCurveWindow', window=True )

###########################################################################################################################################
##################################################    second part of script    ############################################################
###########################################################################################################################################
def cp_showLRA(*args):
    import maya.cmds as mc
    allJointsList= mc.ls(type='joint')
    if allJointsList:
        for i in allJointsList:
            attrToSet= '{0}.displayLocalAxis'.format(i)
            mc.setAttr(attrToSet, 1)


def cp_hideLRA(*args):
    import maya.cmds as mc
    allJointsList= mc.ls(type='joint')
    if allJointsList:
        for i in allJointsList:
            attrToSet= '{0}.displayLocalAxis'.format(i)
            mc.setAttr(attrToSet, 0)


def queryObjList():
    import maya.cmds as mc
    selectedItems= mc.textScrollList('cp_oocInputList', query= True, allItems= True)

    if selectedItems is None:
        selectedItems=[]

    return selectedItems


def cp_show_created_LRA():
    import maya.cmds as mc
    

    if mc.objExists('mopaths_connectedItems_master'):
        connectedNodes= mc.listConnections( 'mopaths_connectedItems_master.createdItems', d=True, s=False )
        if connectedNodes:
            for i in connectedNodes:
                mc.setAttr('{0}.displayLocalAxis'.format(i), 1)

def cp_hide_created_LRA():
    import maya.cmds as mc
    if mc.objExists('mopaths_connectedItems_master'):
        connectedNodes= mc.listConnections( 'mopaths_connectedItems_master.createdItems', d=True, s=False )
        if connectedNodes:
            for i in connectedNodes:
                mc.setAttr('{0}.displayLocalAxis'.format(i), 0)