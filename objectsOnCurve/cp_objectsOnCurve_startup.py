import sys
import os.path

newPath= os.path.normpath("C:\\work\\cp_maya_scripts\\cp_maya_scripts\\objectsOnCurve")

if not newPath in sys.path:
    sys.path.append(newPath)
    
import cp_objectsOnCurve
reload (cp_objectsOnCurve)

cp_objectsOnCurve.main()